<?php

define('PROJECT', '/NFU/');
//SITE_ROOT
$path = $_SERVER['DOCUMENT_ROOT'] . PROJECT;
define('SITE_ROOT', $path);
//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . PROJECT);


define('URL_AMIGABLES', TRUE);

define('MEDIA_ROOT', SITE_ROOT . 'media/');
define('MEDIA_PATH', SITE_PATH . 'media/');
//log

define('USER_LOG_DIR', SITE_ROOT . 'log/user/Site_User_errors.log');
define('GENERAL_LOG_DIR', SITE_ROOT . 'log/general/Site_General_errors.log');
//libs
define('LIBS', SITE_ROOT . 'libs/');
//production
define('PRODUCTION', true);
//modules
define('MODULES_PATH', SITE_ROOT . 'modules/');

//model
define('MODEL_PATH', SITE_ROOT . 'model/');
//view
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');
define('VIEW_PATH_INC_ERROR', SITE_ROOT . 'view/inc/templates_error/');
define('VIEW_PATH_JS', SITE_PATH . 'view/js/');
define('VIEW_ROOT_IMG', SITE_ROOT . 'view/img/');
define('VIEW_PATH_LIB', SITE_PATH . 'view/lib/');
define('VIEW_PATH_IMG', SITE_PATH . 'view/img/');
define('VIEW_PATH_FORM', SITE_PATH . 'view/css_formularios/');
//CSS
define('CSS_PATH', SITE_PATH . 'view/css/');
//font_awesome
define('FONT_PATH', SITE_PATH . 'view/font-awesome/css/');
//resources
define('RESOURCES', SITE_ROOT . 'resources/');
//media
define('MEDIA_PATH', SITE_PATH . 'media/');
//utils
define('UTILS', SITE_ROOT . 'utils/');

//model users
define('FUNCTIONS_USERS', SITE_ROOT . 'modules/users/utils/');
define('MODEL_PATH_USERS', SITE_ROOT . 'modules/users/model/');
define('DAO_USERS', SITE_ROOT . 'modules/users/model/DAO/');
define('BLL_USERS', SITE_ROOT . 'modules/users/model/BLL/');
define('MODEL_USERS', SITE_ROOT . 'modules/users/model/model/');
define('USERS_JS_PATH', SITE_PATH . 'modules/users/view/js/');
define('USERS_CSS_PATH', SITE_PATH . 'modules/users/view/css/');
define('USERS_IMG_PATH', SITE_PATH . 'modules/users/view/img/');

//model login
define('FUNCTIONS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('MODEL_PATH_LOGIN', SITE_ROOT . 'modules/login/model/');
define('DAO_LOGIN', SITE_ROOT . 'modules/login/model/DAO/');
define('BLL_LOGIN', SITE_ROOT . 'modules/login/model/BLL/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');
define('LOGIN_JS_PATH', SITE_PATH . 'modules/login/view/js/');
define('LOGIN_CSS_PATH', SITE_PATH . 'modules/login/view/css/');
define('LOGIN_IMG_PATH', SITE_PATH . 'modules/login/view/img/');

//model INSTALLATION
define('UTILS_INSTALLATION', SITE_ROOT . 'modules/installation/utils/');
define('INSTALLATION_JS_LIB_PATH', SITE_PATH . 'modules/installation/view/libs/');
define('INSTALLATION_JS_PATH', SITE_PATH . 'modules/installation/view/js/');
define('INSTALLATION_CSS_PATH', SITE_PATH . 'modules/installation/view/css/');
define('INSTALLATION_IMG_PATH', SITE_PATH . 'modules/installation/view/img/');
define('MODEL_INSTALLATION', SITE_ROOT . 'modules/installation/model/model/');
define('DAO_INSTALLATION', SITE_ROOT . 'modules/installation/model/DAO/');
define('BLL_INSTALLATION', SITE_ROOT . 'modules/installation/model/BLL/');
define('MODEL_INSTALLATION', SITE_ROOT . 'modules/installation/model/model/');


//model MAIN
define('UTILS_MAIN', SITE_ROOT . 'modules/main/utils/');
define('MAIN_JS_LIB_PATH', SITE_PATH . 'modules/main/view/libs/');
define('MAIN_IMG_PATH', SITE_PATH . 'modules/main/view/img/');
define('MAIN_JS_PATH', SITE_PATH . 'modules/main/view/js/');
define('MAIN_CSS_PATH', SITE_PATH . 'modules/main/view/css/');
define('MODEL_MAIN', SITE_ROOT . 'modules/main/model/model/');
define('DAO_MAIN', SITE_ROOT . 'modules/main/model/DAO/');
define('BLL_MAIN', SITE_ROOT . 'modules/main/model/BLL/');
define('MODEL_MAIN', SITE_ROOT . 'modules/main/model/model/');



/* CONTACT */
define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');
define('CONTACT_CSS_PATH', SITE_PATH . 'modules/contact/view/css/');
define('CONTACT_VIEW_PATH', SITE_ROOT . 'modules/contact/view/');
define('CONTACT_LIB_PATH', SITE_PATH . 'modules/contact/view/libs/');
define('IMG_NFU', SITE_ROOT . 'view/img/logomini.png');

/* MODEL GAMES */
define('FUNCTIONS_GAME', SITE_ROOT . 'modules/games/utils/');
define('MODEL_PATH_GAME', SITE_ROOT . 'modules/games/model/');
define('DAO_GAME', SITE_ROOT . 'modules/games/model/DAO/');
define('BLL_GAME', SITE_ROOT . 'modules/games/model/BLL/');
define('MODEL_GAME', SITE_ROOT . 'modules/games/model/model/');
define('GAME_JS_PATH', SITE_PATH . 'modules/games/view/js/');
define('TIMEPIKER_GAME', SITE_PATH . 'view/lib/jquery-timepicker-master/');


