<?php

function enviar_email($arr) {

    $html = '';
    $subject = '';
    $body = '';
    $ruta = '';
    $return = '';

    switch ($arr['type']) {
      
        case 'alta':
          
            $subject = 'Tu Alta en NFU';
            $ruta = "<a href='" . $_SERVER['HTTP_HOST'] .amigable("?module=users&function=activarUser&aux=A" . $arr['token'], true) . "'>aqu&iacute;</a>";
            $body = 'Gracias por unirte a nuestra aplicación;n<br> Para finalizar el registro, pulsa ' . $ruta;
            break;

        case 'modificacion':
            $subject = 'Tu Nuevo Password en NFU<br>';
            $ruta = "<a href='" . $_SERVER['HTTP_HOST'] .amigable("?module=users&function=activarUser&aux=R" . $arr['token'], true) . "'>aqu&iacute;</a>";
            $body = 'Para recordar tu password pulsa ' . $ruta;
            break;
   case 'recovery':
            $subject = 'Recuperar Contraseña<br>';
$ruta = "<a href='" . $_SERVER['HTTP_HOST'] .amigable("?module=users&function=activarUser&aux=R" . $arr['token'], true) . "'>aqu&iacute;</a>";
            /*$ruta = "<a href='".SITE_PATH . amigable("?module=main&function=begin" . $arr['token'], true) . "'>aqu&iacute;</a>";*/
            $body = 'Para recuperar tu password pulsa ' . $ruta;
            break;
        case 'contact':

            $subject = 'Tu Petición a NFU ha sido enviada';
            $ruta = "<a href='".SITE_PATH . amigable("?module=main&function=begin" . $arr['token'], true) . "'>aqu&iacute;</a>";
            $body = 'Para visitar nuestra web, pulsa ' . $ruta;

            break;

        case 'admin':

            $subject = $arr['inputSubject'];
            $body = 'inputName: ' . $arr['inputName'] . '<br>' .
                    'inputEmail: ' . $arr['inputEmail'] . '<br>' .
                    'inputSubject: ' . $arr['inputSubject'] . '<br>' .
                    'inputMessage: ' . $arr['inputMessage'];

            break;
    }

   

    $html .= "<html>";
    $html .= "<body>";
    $html .= "<h4>" . $subject . "</h4><br>";
    $html .= $body;
    $html .= "<br><br>";
    $html .= "<p>Enviado por NFU</p>";
    $html .= "</body>";
    $html .= "</html>";

    set_error_handler('ErrorHandler');
    try {

        $mail = email::getInstance();

        $mail->name = $arr['inputName'];

        
        if ($arr['type'] === 'admin') {
            $mail->address = "";
            $mail->address = 'nfunosfaltauno@gmail.com';
            $mail->body = $body;
        } elseif ($arr['type'] === 'alta') {
            $mail->address = $arr['inputEmail'];


            $mail->subject = $subject;
            $mail->body = $html;
        }
        elseif ($arr['type'] === 'contact') {
            $mail->address = $arr['inputEmail'];


            $mail->subject = $subject;
            $mail->body = $html;
        }elseif ($arr['type'] === 'recovery') {
                        
            $mail->address = $arr['inputEmail'];
            $mail->subject = $subject;
            $mail->body = $html;
            
        }
    } catch (Exception $e) {
        $return = 0;
    }
    restore_error_handler();

    if ($mail->enviar()) {

        $return = 1;
    } else {
        $return = 0;
    }
    return $return;
}
