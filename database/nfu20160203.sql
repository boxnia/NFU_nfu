CREATE DATABASE  IF NOT EXISTS `nfu` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `nfu`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: nfu
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `ubicacion` varchar(255) DEFAULT NULL,
  `deporte` varchar(255) DEFAULT NULL,
  `hora` varchar(255) DEFAULT NULL,
  `duracion` varchar(255) DEFAULT NULL,
  `inscripcion` varchar(255) DEFAULT NULL,
  `plazas` mediumint(9) DEFAULT NULL,
  `dia` varchar(45) DEFAULT NULL,
  `id_install` mediumint(8) NOT NULL,
  PRIMARY KEY (`id`,`id_install`),
  KEY `id_install` (`id_install`),
  CONSTRAINT `game_ibfk_1` FOREIGN KEY (`id_install`) REFERENCES `installation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` VALUES (1,'TENISTOTAL','ontinyent','tenis','08:00 AM','01:00:00','Gratuio',1,'01/08/2016',1),(2,'TENISTOTAL','ontinyent','tenis','08:00 AM','01:00:00','Gratuio',1,'01/08/2016',2),(3,'VOLEY ONTINYENT','ontinyent','voleibol','09:00 AM','01:00:00','Gratuio',5,'01/08/2016',1),(4,'FUTBOL A TOPE','ontinyent','futbol','08:00 AM','01:00:00','Gratuio',7,'01/08/2016',3),(5,'FUTBOLTOTAL','ontinyent','futbol','08:00 AM','01:00:00','Gratuio',7,'01/08/2016',4),(6,'FUTBOLTOTAL','ontinyent','futbol','08:00 AM','01:00:00','Gratuio',7,'01/08/2016',5),(7,'FUTBOL TOTAL','ontinyent','futbol','08:00 AM','01:00:00','Gratuio',7,'01/09/2016',5),(8,'BALONCESTO A TOPE','ontinyent','baloncesto','08:00 AM','01:00:00','Gratuio',3,'01/09/2016',3),(9,'BALONCESTO YUJU','ontinyent','baloncesto','08:00 AM','01:00:00','Gratuio',4,'01/09/2016',4),(10,'VOLEY JAJA','ontinyent','voleibol','08:00 AM','01:00:00','Gratuio',4,'01/09/2016',1),(11,'VOLEY PARTIDAZO','ontinyent','voleibol','08:00 AM','01:00:00','Gratuio',4,'01/09/2016',1),(12,'VOLEY CHICOS','ontinyent','voleibol','08:00 AM','01:00:00','Gratuio',2,'01/09/2016',2),(13,'TENIS A TOPE','ontinyent','tenis','08:00 AM','01:00:00','2.50',1,'01/08/2016',1),(14,'FOOTBALL','ontinyent','voleibol','08:00 AM','01:00:00','2',4,'01/07/2016',4);
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `installation`
--

DROP TABLE IF EXISTS `installation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `installation` (
  `id` mediumint(8) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `ubicacion` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `valoracion` varchar(100) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `latitud` varchar(255) DEFAULT NULL,
  `longitud` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `installation`
--

LOCK TABLES `installation` WRITE;
/*!40000 ALTER TABLE `installation` DISABLE KEYS */;
INSERT INTO `installation` VALUES (1,'Brett','Buggenhout','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.819381',-0.59618),(2,'Declan','Châtellerault','Lorem ipsum dolor sit amet,','Nivel.svg','installa.png','38.819716',-0.60015),(3,'Quinn','Bergen','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.810252',-0.604248),(4,'Alfonso','Lochristi','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.820384',-0.60163),(5,'Erich','Coleville Lake','Lorem','Nivel.svg','installa.png','38.821571',-0.606716),(6,'Kasper','Lamont','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.820267',-0.611844),(7,'Fuller','Fontaine-Valmont','Lorem','Nivel.svg','installa.png',' 38.825115',-0.598111),(8,'Joel','Baie-Comeau','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.988235',-1.86905),(9,'Fitzgerald','Romano d\'Ezzelino','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(10,'Vincent','Souvret','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(11,'Garth','Wyoming','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(12,'Micah','Glimes','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(13,'Tanek','Moerzeke','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(14,'Hall','Barra do Corda','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(15,'Eaton','Macklin','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(16,'Wayne','Wörgl','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(17,'Guy','Soverzene','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(18,'Armando','Kearny','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(19,'Price','Charleville-Mézières','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(20,'Timon','Buti','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(21,'Keefe','Lesve','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(22,'Seth','Meer','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(23,'Brent','Koningshooikt','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(24,'Laith','Tarvisio','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(25,'George','Sanluri','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(26,'Ulysses','Cantley','Lorem ipsum dolor sit amet,','Nivel.svg','installa.png','38.79913404632731',-0.618528),(27,'Jared','Marlborough','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(28,'Asher','Habergy','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(29,'Darius','Castlegar','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(30,'Zahir','Tournefeuille','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(31,'Harding','Caramanico Terme','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(32,'Kelly','Laramie','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(33,'Martin','San Rafael Abajo','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(34,'Moses','Rimbey','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.815101',-0.884399),(35,'Chaim','Fort Providence','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(36,'Oscar','Lourdes','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(37,'Timon','Lauregno/Laurein','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(38,'Blake','Mannheim','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(39,'Judah','Gomz?-Andoumont','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(40,'Kevin','Hoorn','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(41,'Malachi','Dillenburg','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(42,'Quamar','Pomarico','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(43,'Tucker','Robelmont','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(44,'Bernard','Pemberton','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(45,'Hilel','Anderlecht','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(46,'Harding','Hody','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(47,'Howard','Rattray','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(48,'Chadwick','Kakinada','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(49,'Kennan','Alexandra','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(50,'Rafael','Bowden','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(51,'Theodore','Schwaz','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(52,'Sebastian','Moe','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(53,'Philip','Lampertheim','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(54,'Bevis','Torino','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(55,'Branden','Crescentino','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(56,'Jordan','Cartagena','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(57,'Bert','Wetzlar','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(58,'Ulric','Granada','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(59,'Benedict','?mamo?lu','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(60,'Nero','Shaki','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(61,'Barry','Sokoto','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(62,'Jared','Tarvisio','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(63,'Zephania','Boncelles','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(64,'Caesar','G?rompont','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(65,'Nathan','Northumberland','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(66,'Harding','Limena','Lorem ipsum dolor sit amet,','Nivel.svg','installa.png','38.79913404632731',-0.618528),(67,'Hector','Mesa','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(68,'Anthony','Ujjain','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(69,'Justin','Portobuffolè','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(70,'Steel','Hines Creek','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(71,'Dante','Cherbourg-Octeville','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(72,'Ivan','Gulfport','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(73,'Kasper','Sint-Gillis','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(74,'Addison','Amaro','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(75,'Garrett','Town of Yarmouth','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(76,'Callum','Kitchener','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(77,'Reese','Lafayette','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(78,'Honorato','Levallois-Perret','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(79,'Reese','Pelago','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(80,'Baxter','Wiekevorst','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(81,'Erasmus','Zuccarello','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(82,'Gil','Fort Wayne','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(83,'Abdul','Verrayes','Lorem ipsum dolor sit amet, consectetuer adipiscing elit.','Nivel.svg','installa.png','38.79913404632731',-0.618528),(84,'Garrett','Hantes-Wih?ries','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(85,'Fitzgerald','Gijzegem','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(86,'Solomon','Port Coquitlam','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(87,'Talon','Poederlee','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(88,'Malachi','Dilsen-Stokkem','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(89,'Tarik','Ramagundam','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed','Nivel.svg','installa.png','38.79913404632731',-0.618528),(90,'Prescott','Lustenau','Lorem ipsum dolor sit amet, consectetuer adipiscing','Nivel.svg','installa.png','38.79913404632731',-0.618528),(91,'Alfonso','Dreieich','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(92,'Russell','Cabano','Lorem ipsum','Nivel.svg','installa.png','38.79913404632731',-0.618528),(93,'Raja','Rosarno','Lorem','Nivel.svg','installa.png','38.79913404632731',-0.618528),(94,'Burton','Yellowhead County','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(95,'Rooney','Surrey','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(96,'Cadman','High Level','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(97,'Dale','Amlwch','Lorem ipsum dolor sit','Nivel.svg','installa.png','38.79913404632731',-0.618528),(98,'Amal','Treppo Carnico','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur','Nivel.svg','installa.png','38.79913404632731',-0.618528),(99,'Gil','Juneau','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(100,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(101,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(102,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(103,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(104,'Amery','Rossignol','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.79913404632731',-0.618528),(105,'Amery','Ontinyent','Lorem ipsum dolor sit amet, consectetuer','Nivel.svg','installa.png','38.833277',-0.568125);
/*!40000 ALTER TABLE `installation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `numero` varchar(100) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `provincia` varchar(100) DEFAULT NULL,
  `todos` tinyint(1) DEFAULT NULL,
  `futbol` tinyint(1) DEFAULT NULL,
  `baloncesto` tinyint(1) DEFAULT NULL,
  `voleibol` tinyint(1) DEFAULT NULL,
  `tenis` tinyint(1) DEFAULT NULL,
  `padel` tinyint(1) DEFAULT NULL,
  `nivel` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fecha_nac` varchar(10) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  `avatar` varchar(1000) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `codigo` varchar(500) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`nombre`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('damian',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'tutramite.internet@gmail.com ','$2y$10$7q19wfNRiqbCTqxRG5pzwOX36dAfaPxrCE3HmUtuRFUh8VchUtsCS','','',NULL,NULL,NULL,'44ee5ef90e742b4d83b80e0ebb6517d2','1',NULL),('DamianDonat','','','','','',0,0,0,0,0,0,'','','','','2016-02-02','http://www.gravatar.com/avatar/9b9ed6162871fdf912d8d38c3f19b3be9b9ed6162871fdf912d8d38c3f19b3be?s=80','','4865340063','05b972e5c0af427b4cee4781e6ce96ed','1','normal'),('damiandonat','','','','','',0,0,0,0,0,0,'','damiandonatwork@gmail.com','$2y$10$MIQHGA4qVEwtE7dkEewebORQuN0J8uv2i0HDDqXLRDb8fySbzIKVK','01/11/1996','2016-01-26','default-avatar.png','','0','09b616c47a59cafc9fd7b7212d38e5a5','1','normal'),('laura','masculino','','679440898','','',1,1,0,0,1,0,'bajo','lauragomoreno@gmail.com','$2y$10$ifQan1Ajx4w/JrlB/V8OHuMu53/MKTjSK5XQX7T7FqYuL0IHvmPzW','01/07/1981','2016-01-30','http://51.254.97.198/NFU/media/ajax-loader.gif','ES','','3de6c06384be2b0927297a7a5b54b286','1','normal'),('Mati May','masculino','','625860765','','',1,1,0,0,1,0,'bajo','boxnia@hotmail.com','','02/01/1996','2016-01-30','https://scontent.xx.fbcdn.net/hprofile-xfp1/v/t1.0-1/p50x50/12376761_10153809354306112_4281860071291933618_n.jpg?oh=1af4b4aec510a4547eb67d66281e24a8&oe=576F6A75','ES','10153898306266112','2f620df2f55922de0bca9aff8c8ec330','1','normal'),('miguel','','','','','',0,1,1,0,0,0,'','miguel.gh96@gmail.com','$2y$10$xurWRndwpU2bEKWZvnaMn.5T/VxGvU6psOZYAGjIYQS8G3djlQmem','01/06/1996','2016-01-29','default-avatar.png','ES','0','3ea399b4abc4bce5d845ddb2f6c69a57','1','normal'),('NFaltaUno','','','625860765','','',0,0,0,0,0,0,'','lauragomoreno@gmail.com','','02/23/1996','2016-02-02','http://www.gravatar.com/avatar/9423481bf37612c6a0de82116be228269423481bf37612c6a0de82116be22826?s=80&d=identicon&r=g','ES','4644441855','49048900f186cc21786a0d0107b727e3','1','normal'),('pee','','','','','',0,0,0,0,0,0,'','nfunosfaltauno@gmail.com','$2y$10$xuS5d7oYHfY3oKWmXOCgYeQ3s4eciglRo55TCFiRFuMtqY4ezOmXm','01/20/1981','2016-01-28','default-avatar.png','','0','80209f4e380914e765d45559e3cf2439','0','normal'),('peerrr','','','','','',0,0,0,0,0,0,'','xx@gmail.com','$2y$10$fdVrMQlA6fP6dwd2PdrdY.I3TOal.uBz8ZmcN5oNvEpV2z0Wipr3W','01/20/1981','2016-01-28','default-avatar.png','','0','4b7ee032ce8d78cbff70ea2245b0faa8','0','normal'),('vicent','','','','','',0,0,0,0,0,0,'','vicentcortez@gmail.com','$2y$10$T3MkSK0Vq2VjDk4raLWB5.UCVLaBm9RaS7pt7AHK/0jVZWLd1rnVS','01/09/1997','2016-01-27','default-avatar.png','','0','e9b10a29e8edba004c68c2d91b644077','1','normal');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'nfu'
--

--
-- Dumping routines for database 'nfu'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-03 11:04:22
