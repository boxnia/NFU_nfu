

$(document).ready(function () {
    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, {timeout: 3000});
        google.maps.event.addDomListener(window, 'load', onSuccess);
    } else {
        document.addEventListener("deviceready", onDeviceReady, false);
        var watchID = null;
        // device APIs are available
        //
        function onDeviceReady() {
            // Get the most accurate position updates available on the
            // device.
            // var options = {enableHighAccuracy: true};
            navigator.geolocation.getCurrentPosition(onSuccess, onError, {enableHighAccuracy: false, timeout: 3000});
        }
    }

    function onSuccess(position) {
        var current_page = 1;
///////////////////////lat y long geolocalizado
        var lat = position.coords.latitude;
        var lang = position.coords.longitude;
        // console.log(lat);

        var myLatlng = new google.maps.LatLng(lat, lang);
////////////////////////////mapa a cargar en map con la geolocaclizacion


        //initialize();
        //codeLatLng(lat, lang, myLatlng);
/////////////////////datos para calcular distancia al usuario de la partida
        var data = {
            "lat_position": lat,
            "long_position": lang,
            "p": current_page,
        }

        var data_coord = JSON.stringify(data);
        //alert(data_coord);


        $.post("/main/load_coordinates_near",
                {coord: data_coord},
        function (data) {
            // console.log(response)

            console.log(data);
            //console.log(response.succes);


            var game = data.game;
            console.log(game);
            for (i = 0; i < game.length; i++) {

                var first_point = new google.maps.LatLng(lat, lang);
                var second_point = new google.maps.LatLng(game[i].latitud, game[i].longitud);
                var distancia = google.maps.geometry.spherical.computeDistanceBetween(first_point, second_point);
                var marker, i;
                var icon = "";
                //console.log(distancia);

                if (distancia < 2000) {
                    var deporte = game[i].deporte;
                    console.log(deporte);
                    if (game[i].deporte === 'tenis') {
                        icon = ("https://image.freepik.com/foto-gratis/raquetas-y-pelotas-de-tenis_21064561.jpg");
                    } else if (game[i].deporte === 'futbol') {

                        icon = ("http://www.campuspalomino.com/wp-content/uploads/2014/04/balon-futbol-campo-casas-1.jpg");
                    }
                    else if (game[i].deporte === 'baloncesto') {

                        icon = ("http://www.campuspalomino.com/wp-content/uploads/2014/04/balon-futbol-campo-casas-1.jpg");
                    }
                    else if (game[i].deporte === 'padel') {

                        icon = ("http://www.campuspalomino.com/wp-content/uploads/2014/04/balon-futbol-campo-casas-1.jpg");
                    } else if (game[i].deporte === 'voleibol') {
                        icon = ("http://static4.rutinasentrenamiento.com/wp-content/uploads/volley.jpg");
                    }











                    var games = ' <div class="col-sm-4"> \n\
                    <div class="form-box">\n\
                        <div class="form-bottom contact-form">\n\
                    <label id="posicion_partida" class="label label-default" >' + game[i].ubicacion + '</label>\n\
                    <img class="img-portfolio img-responsive" src="' + icon + '" alt="juego">\n\
                         </div>\
                     <div class="form-bottom contact-form">\n\
                            <div class="form-bottom contact-form">\n\
                            <div class="col-xs-6 col-md-6" id="propiedades">\n\
                                    <div class="col-xs-6 col-md-6 ">\n\
                                        <label class="blue_leter">Nivel:</label>\
                                     </div>\
                                    <div class="col-xs-6 col-md-6 ">\
                                        <img class="" src="' + ruta_image(game[i].valoracion) + '" height="42" width="42">\n\
                                    </div>\
                             </div>\
                             <div class="col-xs-6 col-md-6" id="propiedades">\n\
                                <div class="col-xs-6 col-md-6 ">\n\
                                    <label class="blue_leter">Nos falta:</label>\
                                </div>\
                                <div class="col-xs-6 col-md-6 ">\
                                         <img class="" src="' + ruta_image(game[i].valoracion) + '" height="42" width="42">\n\
                                </div>\
                             </div>\
                      </div>\
                                 <div class="form-bottom contact-form">\
                                       <div class="col-xs-6 col-xs-4" id="bottom_partidas">\
                                               <label>Dia:</label>\
                                                 <label  >' + game[i].dia + '</label>\
</div > \
                                                    <div class="form-bottom contact-form">\
                                       <div class="col-xs-6 col-xs-4" id="bottom_partidas">\
                                               <label>Inicio:</label>\
                                                 <label  >' + game[i].hora + '</label>\
</div >\
                                <div class="col-xs-6 col-xs-4" id="btn_partida">\
                                 <a  href=""><label id="juega">JUEGA</a>\
                                 </div >\
                                 </div >\
                                   </div >\
                    </div>';
                    $(games).hide().appendTo('#results').fadeIn(1000);
                    //$(games).hide().appendTo('#results').fadeIn(1000);

                   // var lati = game[i].latitud;
                    //var longi = game[i].longitud;
                }
            }

        }, "json")
                .fail(function (xhr, textStatus, errorThrown) {

                    if (xhr.status === 0) {
                        console.log('Not connect: Verify Network.');
                    } else if (xhr.status == 404) {
                        console.log('Requested page not found [404]');
                    } else if (xhr.status == 500) {
                        console.log('Internal Server Error [500].');
                    } else if (textStatus === 'parsererror') {
                        console.log('Requested JSON parse failed.');
                    } else if (textStatus === 'timeout') {
                        console.log('Time out error.');
                    } else if (textStatus === 'abort') {
                        console.log('Ajax request aborted.');
                    } else {
                        console.log('Uncaught Error: ' + xhr.responseText);
                    }


                });
    }



    function onError(error) {
        //alert("hola");
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.")
                break;
            case error.TIMEOUT:
                alert("Tu gps está apagado!!")
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.")
                break;
        }
    }




});
function ruta_image(image) {
    //alert(image);
    var project = "http://51.254.117.90/NFU/media/"
    var ruta = project + image;
    return ruta;
}

