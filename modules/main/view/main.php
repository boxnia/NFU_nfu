<script type="text/javascript" src="<?php echo MAIN_JS_PATH ?>main.js" ></script>
<link rel="stylesheet" href="<?php echo CSS_PATH ?>partidas_main.css">
<section id="match" class="match bg-primary">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-10 col-lg-offset-1">
                <h2 class="last_match">PARTIDAS</h2>
                <hr class="small">
                <div class="row">
 <div class="form-group">
                        <div id="results"></div>
                          </div>
                </div>
                <br>
                

                <!-- row-->


                <a href="#" class="btn btn-dark">Ver más partidas</a>
            </div><!-- col-lg-10 -->
        </div><!-- row text-->
    </div><!-- container -->


</section>


<!-- About -->
<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="main_letrero">NOS FALTA UNO</h2>
                <p class="lead">¿Nunca puedes realizar tu deporte favorito porque no encuentras personas con 

                    quien practicarlo? :( <strong>“Nos Falta Uno”</strong> te ayudará a encontrarla :) 

                    Simplemente configurando unos fáciles parámetros, “Nos Falta Uno” te 

                    facilitará información de personas que como tú, necesitan de un compañero de 

                    equipo a una distancia próxima. <a target="_blank" href=""></a>.</p>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<!-- Callout -->
<!--<aside class="callout">
    <div class="text-vertical-center">
        <h1>Vertically Centered Text</h1>
    </div>
</aside>

<!-- Portfolio -->
<hr class="blue">
<section id="portfolio">
    <div class="container">
        <div class="row">

            <div class="row">
                <div class="col-md-6">
                    <div class="portfolio-item">
                        <div class="col-lg-10 col-lg-offset-1 text-center">
                            <h2 class="last_match">MEJORES USUARIOS</h2>
                            <hr class="small">
                        </div>
                        <div class="anchor"></div>
                        <div class="row" id="index">
                            <div class="col-md-6" id="index1"><img class="img-avatar img-responsive" src="<?php echo MAIN_IMG_PATH ?>Avatar.png"></div>
                            <div class="col-md-6" id="index2"><img class="img-avatar img-responsive" src="<?php echo MAIN_IMG_PATH ?>Avatar.png"></div>
                            <div class="col-md-6" id="index3"><img class="img-avatar img-responsive" src="<?php echo MAIN_IMG_PATH ?>Avatar.png"></div>
                        </div>                            <a>
                            <center><img class="img-podium img-responsive" src="<?php echo MAIN_IMG_PATH ?>Podio.svg"></center>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portfolio-item">
                        <div class="col-lg-10 col-lg-offset-1 text-center">
                            <h2 class="last_match">MEJORES INSTALACIONES</h2>
                            <hr class="small">
                        </div>
                        <div class="anchor"></div>
                        <div class="row" id="index">
                            <div class="col-md-6" id="index1"> <img class="img-avatar img-responsive" src="<?php echo MAIN_IMG_PATH ?>Avatar.png"></div>
                            <div class="col-md-6" id="index2"><img class="img-avatar img-responsive" src="<?php echo MAIN_IMG_PATH ?>Avatar.png"></div>
                            <div class="col-md-6" id="index3"><img class="img-avatar img-responsive" src="<?php echo MAIN_IMG_PATH ?>Avatar.png"></div>
                        </div>       
                        <a>
                            <center><img class="img-podium img-responsive" src="<?php echo MAIN_IMG_PATH ?>Podio.svg"></center>
                        </a>
                    </div>
                </div>

            </div>
            <!-- /.row (nested) -->

        </div>
        <!-- /.col-lg-10 -->
    </div>
    <!-- /.row -->
    <!-- /.container -->
</section>
<hr class="blue">
<aside id="white">

    <!-- slider -->
    <div id="demo">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h2 class="last_match">PATROCINADORES</h2>
                    <hr class="small">
                    <div class="span12">

                        <div id="owl-demo" class="owl-carousel">

                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/1.jpg" alt="Owl Image"></div>
                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/2.jpg"></div>
                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/3.jpg"></div>
                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/4.jpg"></div>
                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/5.jpg"></div>
                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/6.jpg"></div>
                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/7.jpg"></div>
                            <div class="item"><img src="<?php echo VIEW_PATH_IMG ?>logos/8.jpg"></div>

                        </div>
                    </div>
                </div>

            </div>
            </aside>
            <hr class="blue">
            <!-- Call to Action -->
            <!--<aside class="call-to-action bg-primary">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h3>The buttons below are impossible to resist.</h3>
                            <a href="#" class="btn btn-lg btn-light">Click Me!</a>
                            <a href="#" class="btn btn-lg btn-dark">Look at Me!</a>
                        </div>
                    </div>
                </div>
            </aside>
            
            <!-- Map -->
            <!--<section id="contact" class="map">
                <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
                <br />
                <small>
                    <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>
                </small>
                </iframe>
            </section>-->





