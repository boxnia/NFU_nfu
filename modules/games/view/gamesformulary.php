<script type="text/javascript" src="<?php echo GAME_JS_PATH ?>games.js" ></script>

<h4>Formulario Partidas</h4>
<form name="formulary_game" id="formulary_prod">

    <div class="input-form">
        <?php
        ?>
        <table width="80%" border="0" cellspacing="0" cellpadding="0">          
            </tr>
            <!--game name-->
            <tr>               
            <input name="gamename" type="text" width="75%" id="gamename" class="form-control" placeholder="Nombre de la patida" value="<?php echo $_POST ? $_POST['nombre'] : ""; ?>">
            <div id="e_gamename" style="color: #ff0000"></div>

            </tr><!--fin game name-->

            <!--time start-->
            <tr>                
            <input name="times_start" type="text" id="time_start" width="75%" class="form-control" placeholder="Hora de inicio" value="<?php echo $_POST ? $_POST['hora'] : ""; ?>">
            <span id="time_start_e" value="<?php echo $error ? $error['codigo'] : ""; ?>"></span>
            <div id="e_time_start" style="color: #ff0000"></div>
            </tr><!--fin time start-->

            <!--duration-->
            <tr>                
            <input name="durations" type="text" id="duration" width="75%" class="form-control" placeholder="Duracion" value="<?php echo $_POST ? $_POST['duracion'] : ""; ?>">
            <span id="duration_e" value="<?php echo $error ? $error['codigo'] : ""; ?>"></span>
            <div id="e_duration" style="color: #ff0000"></div>
            </tr><!--fin duration-->

            <!--deportes-->
            <tr>                
                <td width="50%">
                    <select name="sport" id="type_sport" class="form-control" readonly>
                        <optgroup label="deporte">
                            <option value="futbol">Futbol</option>
                            <option value="baloncesto">Baloncesto</option>
                            <option value="voleibol">Voleibol</option>
                            <option value="tenis">Tenis</option>
                            <option value="padel">Padel</option>
                        </optgroup>
                    </select>
                </td>
            </tr><!--fin deportes-->

            <!--tipo de pago-->
            <tr>
                <td>Entrada</td>
                <td>Gratuita<input name="enter" type="radio" value="0" checked>
                    Pago<input name="enter" type="radio" value="1">                    
                </td>
            </tr><!--fin tipo de pago-->

            <!--si es un tipo de pago-->
            <tr>                
                <td><input name="price" type="text" width="75%" id="price_cash" class="form-control" placeholder="Precio de la entrada" value="<?php echo $_POST ? $_POST['precioentrada'] : ""; ?>"></td>
                <div id="e_price_cash" style="color: #ff0000"></div>
            </tr><!--fin de si es un tipo de pago-->

            <!--dia de la partida-->
            <tr>

                <td>
                    <input id="day_game" type="text" name="days_game" placeholder="Dia de la partida" readonly value="<?php echo $_POST ? $_POST['daygame'] : ""; ?>">
                    <div id="e_day_game" style="color: #ff0000"></div>
                    <div id="result"></div>
                </td>
                
            </tr><!--fin dia de la partida-->  
            
            <!--numero de plazas-->
            <tr>

                <td>                    
                    <input name="places_number" type="text" width="75%" id="places_number" class="form-control" placeholder="Numero de plazas" value="<?php echo $_POST ? $_POST['plazas'] : ""; ?>">
                    <div id="e_places_number" style="color: #ff0000"></div>                    
                </td>
                
            </tr><!--fin numero de plazas-->
            
            <!--comentarios zona de encuentro-->
            <textarea placeholder="Describe la zona de encuentro..." maxlength="255" rows="4" cols="50"></textarea>
            <!--fin comentarios zona de encuentro-->
        </table>
        <span class="input-group-btn">            
            <center><input type="button" class="btn btn-danger" name="boton" id="boton" value="Enviar"></center>
        </span>
    </div>
</form>
<br>