jQuery.fn.LlenarLimpiarCampos = function () {
    this.each(function () {        

        if ($("#gamename").attr("value") === 0) {
            $("#gamename").attr("value", "Introduzca un nombre de partida");
        }
        $("#gamename").focus(function () {
            if ($("#gamename").attr("value") == "Introduzca un nombre de partida") {
                $("#gamename").attr("value", "");
            }
        });

        $("#gamename").blur(function () {
            if ($("#gamename").attr("value") == "") {
                $("#gamename").attr("value", "Introduzca un nombre de partida");
            }
        });
        
        if ($("#time_start").attr("value") === 0) {
            $("#time_start").attr("value", "Introduzca la hora de inicio");
        }
        $("#time_start").focus(function () {
            if ($("#time_start").attr("value") == "Introduzca la hora de inicio") {
                $("#time_start").attr("value", "");
            }
        });
        $("#time_start").blur(function () {
            if ($("#time_start").attr("value") == "") {
                $("#time_start").attr("value", "Introduzca la hora de inicio");
            }
        });

        if ($("#duration").attr("value") === 0) {
            $("#duration").attr("value", "Introduzca la duración de la partida");
        }
        $("#duration").focus(function () {
            if ($("#duration").attr("value") == "Introduzca la duración de la partida") {
                $("#duration").attr("value", "");
            }
        });
        $("#duration").blur(function () { //Onblur se activa cuando el usuario retira el foco
            if ($("#duration").attr("value") == "") {
                $("#duration").attr("value", "Introduzca la duración de la partida");
            }
        });

        if ($("#price_cash").attr("value") === 0) {
            $("#price_cash").attr("value", "Introduzca un precio de la entrada");
        }
        $("#price_cash").focus(function () {
            if ($("#price_cash").attr("value") == "Introduzca un precio de la entrada") {
                $("#price_cash").attr("value", "");
            }
        });
        $("#price_cash").blur(function () {
            if ($("#price_cash").attr("value") == "") {
                $("#price_cash").attr("value", "Introduzca un precio de la entrada");
            }
        });


        if ($("#day_game").attr("value") === 0) {
            $("#day_game").attr("value", "Introduzca la fecha de la partida");
        }
        $("#day_game").focus(function () {
            if ($("#day_game").attr("value") == "Introduzca la fecha de la partida") {
                $("#day_game").attr("value", "");
            }
        });
        $("#day_game").blur(function () {
            if ($("#day_game").attr("value") == "") {
                $("#day_game").attr("value", "Introduzca la fecha de la partida");
            }
        });
                
        if ($("#places_number").attr("value") === 0) {
            $("#places_number").attr("value", "Introduzca las plazas disponibles");
        }
        $("#places_number").focus(function () {
            if ($("#places_number").attr("value") == "Introduzca las plazas disponibles") {
                $("#places_number").attr("value", "");
            }
        });
        $("#places_number").blur(function () {
            if ($("#places_number").attr("value") == "") {
                $("#places_number").attr("value", "Introduzca las plazas disponibles");
            }
        });

    });
    return this;
};


function validate_provincia(provincia) {
    if (provincia == null) {
        return 'default_provincia';
    }
    if (provincia.length == 0) {
        return 'default_provincia';
    }
    if (provincia === 'Selecciona una Provincia') {
        //return 'default_provincia';
        return false;
    }
    if (provincia.length > 0) {
        var regexp = /^[a-zA-Z0-9, ]*$/;
        return regexp.test(provincia);
    }
    return false;
}
function validate_poblacion(poblacion) {
    if (poblacion == null) {
        return 'default_poblacion';
    }
    if (poblacion.length == 0) {
        return 'default_poblacion';
    }
    if (poblacion === 'Selecciona una Poblacion') {
        //return 'default_poblacion';
        return false;
    }
    if (poblacion.length > 0) {
        var regexp = /^[a-zA-Z/, -'()]*$/;
        return regexp.test(poblacion);
    }
    return false;
}


$(document).ready(function () {
    //alert("hola");
    $('#price_cash').hide();
    var name_game = /^\D{3,30}$/;
    var time_game = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] (AM|PM)$/;
    var duration_game = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0|3][0]:[0][0]$/;
    var price_game = /^[0-9]+(\.[0-9]+)?$/;
    var places_game = /^([0-9]+)$/;  
    var day_game = /\d{2}.\d{2}.\d{4}$/;

    $.datepicker.setDefaults($.datepicker.regional["es"]);
    $("#day_game").datepicker();    
    $('#time_start').timepicker({ 'timeFormat': 'h:i A' });
    $('#duration').timepicker({ 'timeFormat': 'H:i:s' });       
    $("input[name='enter']:radio").change(function(){
        var price=$("input[name='enter']:checked").val();         
        if(price == 1){
            $('#price_cash').show();
        }else{
            $('#price_cash').hide();
        }
    }); 

    $('#boton').click(function () {
        //alert("hola");
        validate_game();
    });


     
    $.post("/games/load_data_game", {"load_data_game":true},
        
            function (response) {
                //alert(response.prod);
                console.log(response.game);
                if (response.game === "") {
                    $("#gamename").val('');
                    $("#time_start").val('');
                    $("#duration").val('');
                    $("#price_cash").val('');
                    $("#day_game").val('');
                    $("#places_number").val('');
                    /*$("input[type=checkbox]").removeAttr("checked");*/

                } else {
                    $("#gamename").attr("value", response.game.name);
                    $("#time_start").attr("value", response.game.time);
                    $("#duration").attr("value", response.game.duration);
                    $("#price_cash").attr("value", response.game.pricecash);
                    $("#day_game").attr("value", response.game.day);
                    $("#places_number").attr("value", response.game.places);                    
                }
            }, "json");

    $(this).LlenarLimpiarCampos(); //siempre que creemos un plugin debemos llamarlo, sino no funcionará

    

    $("#gamename,#time_start,#duration,#price_cash,#day_game,#places_number").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#gamename").keyup(function () {
        if ($(this).val() != "" && name_game.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#time_start").keyup(function () {

        if ($(this).val() != "" && time_game.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#duration").keyup(function () {

        if ($(this).val() != "" && duration_game.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#price_cash").keyup(function () {

        if ($(this).val() != "" && price_game.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#day_game").keyup(function () {

        if ($(this).val() != "" && day_game.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#places_number").keyup(function () {

        if ($(this).val() != "" && places_game.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    

    /*load_countries_v1();
    $("#provincia").empty();
    $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');
    $("#provincia").prop('disabled', true);
    $("#poblacion").empty();
    $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');
    $("#poblacion").prop('disabled', true);

    $("#pais").change(function () {
        var pais = $(this).val();
        var provincia = $("#provincia");
        var poblacion = $("#poblacion");

        if (pais !== 'ES') {
            provincia.prop('disabled', true);
            poblacion.prop('disabled', true);
            $("#provincia").empty();
            $("#poblacion").empty();
        } else {
            provincia.prop('disabled', false);
            poblacion.prop('disabled', false);
            load_provincias_v1();
        }//fi else
    });*/

    load_provincias_v1();
    $("#provincia").change(function () {
        var prov = $(this).val();
        if (prov > 0) {
            load_poblaciones_v1(prov);
        } else {
            $("#poblacion").prop('disabled', false);
        }
    });

});


function validate_game() {   
    
    var name = $("#gamename").val();
    var time = $("#time_start").val();
    var duration = $("#duration").val();   
    var price1 = $("input[name='enter']:checked").val();
    if (price1 == 1) {
        var pricecash = $("#price_cash").val(); /*solo si esta seleccionado el precio*/
    } else {
        var pricecash = "Gratuio";
    }
    var places = $("#places_number").val();
    var day = $("#day_game").val();
    var sport = $("#type_sport").val();
    

    /*Regular expressions*/
    var name_game = /^\D{3,30}$/;
    var time_game = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] (AM|PM)$/;
    var duration_game = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0|3][0]:[0][0]$/;
    var price_game = /^[0-9]+(\.[0-9]+)?$/;
    var places_game = /^([0-9]+)$/;  
    var day_game = /\d{2}.\d{2}.\d{4}$/;
    var result = true;

    $(".error").remove();

    if ($("#gamename").val() == "" || !name_game.test($("#gamename").val()) || $("#gamename").val() == "Introduzca un nombre de partida") {
        $("#gamename").focus().after("<span class='error'>Introduzca un nombre de partida</span>");
        result = false;
        return;

    }
    if ($("#time_start").val() == "" || !time_game.test($("#time_start").val()) || $("#time_start").val() == "Introduzca la hora de inicio") {
        $("#time_start").focus().after("<span class='error'>Introduzca la hora de inicio</span>");
        result = false;
        return;


    }
    if ($("#duration").val() == "" || !duration_game.test($("#duration").val()) || $("#duration").val() == "Introduzca la duración de la partida") {
        $("#duration").focus().after("<span class='error'>Introduzca la duración de la partida</span>");
        result = false;
        return;


    }
    if (pricecash != "Gratuio"){
        if ($("#price_cash").val() == "" || !price_game.test($("#price_cash").val()) || $("#price_cash").val() == "Introduzca un precio de la entrada") {
            $("#price_cash").focus().after("<span class='error'>Introduzca un precio de la entrada</span>");
            result = false;
            return;
        }
    
    }
       
    if ($("#places_number").val() == "" || !places_game.test($("#places_number").val()) || $("#places_number").val() == "Introduzca las plazas disponibles") {
        $("#places_number").focus().after("<span class='error'>Introduzca las plazas disponibles</span>");
        result = false;
        return;
    }
    
    if ($("#day_game").val() == "" || !day_game.test($("#day_game").val()) || $("#day_game").val() == "Introduzca la fecha de la partida") {
        $("#day_game").focus().after("<span class='error'>Introduzca la fecha de la partida</span>");
        result = false;
        return;


    }
    


    if (result) {

        /*Colocamos aqui los if para saber si debemos colocar la privincia y poblacion como default*/
        /*Estas lineas las hemos colocado ahora*/
        /*var provincia = $("#provincia").val();
        var poblacion = $("#poblacion").val();  */
        var provincia = $("#provincia option:selected" ).text();
        var poblacion = $("#poblacion option:selected" ).text();
        /*if (provincia == null) {
            provincia = 'default_provincia';
        } else if (provincia.length == 0) {
            provincia = 'default_provincia';
        } else if (provincia === 'Selecciona una Provincia') {
            return 'default_provincia';
        }

        if (poblacion == null) {
            poblacion = 'default_poblacion';
        } else if (poblacion.length == 0) {
            poblacion = 'default_poblacion';
        } else if (poblacion === 'Selecciona una Poblacion') {
            return 'default_poblacion';
        }*/                

        /*Pasamos los datos a un json*/        

        var data = {"name": name, "time": time, "duration": duration, "pricecash": pricecash, "places": places, "day":day, "sport":sport, "provincia":provincia,
        "poblacion":poblacion};
        //alert(data.tipoproducto + "linea 263 products.js");
        var data_games_JSON = JSON.stringify(data);       
        console.log(data_games_JSON);
        $.post("/games/up_games", {data_games_JSON: data_games_JSON},
        function (response) {
            console.log(response);
            /*alert(response);*/
            if (response.success) {
                console.log(response);
                window.location.href = response.redirect;
                /*alert(response.redirect);*/
            }

        }, "json").fail(function (xhr) {

            xhr.responseJSON = JSON.parse(xhr.responseText);

            //$("#e_avatar").html(xhr.responseJSON.avatar);
            //$("#e_avatar").focus().after("<span class='error'>"+response.error.avatar+"</span>");

            /*if (!(xhr.responseJSON.success1)) {
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
            }*/            
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.name !== undefined && xhr.responseJSON.error.name !== null) {
                    //$("#e_tipoproducto").text(xhr.responseJSON.error.tipoproducto);
                    $("#e_gamename").focus().after("<span class='error'>" + xhr.responseJSON.error.name + "</span>");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.time !== undefined && xhr.responseJSON.error.time !== null) {
                    //$("#e_nombreproducto").text(xhr.responseJSON.error.nombreproducto);
                    $("#e_time_start").focus().after("<span class='error'>" + xhr.responseJSON.error.time + "</span>");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.duration !== undefined && xhr.responseJSON.error.duration !== null) {
                    //$("#e_codigo").text(xhr.responseJSON.error.codigo);
                    $("#e_duration").focus().after("<span class='error'>" + xhr.responseJSON.error.duration + "</span>");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.pricecash !== undefined && xhr.responseJSON.error.pricecash !== null) {
                    //$("#e_color").text(xhr.responseJSON.error.color);
                    $("#e_price_cash").focus().after("<span class='error'>" + xhr.responseJSON.error.pricecash + "</span>");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.day !== undefined && xhr.responseJSON.error.day !== null) {
                    //$("#date_exit").text(xhr.responseJSON.error.date_exit);
                    $("#e_day_game").focus().after("<span class='error'>" + xhr.responseJSON.error.day + "</span>");
                }
            }
            if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                if (xhr.responseJSON.error.places !== undefined && xhr.responseJSON.error.places !== null) {
                    //$("#arrival_date").text(xhr.responseJSON.error.arrival_date);
                    $("#e_places_number").focus().after("<span class='error'>" + xhr.responseJSON.error.places + "</span>");
                }
            }            

        });
    }
}



function load_provincias_v2() {
    
    $.get("/resources/provinciasypoblaciones.xml", function (xml) {
        
	    $("#provincia").empty();
	    $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');
	            
        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var nombre = $(this).find('nombre').text();
            $("#provincia").append("<option value='" + id + "'>" + nombre + "</option>");
        });
    })
    .fail(function() {
        alert( "error load_provincias" );
    });
}

function load_provincias_v1() { //provinciasypoblaciones.xml - xpath
    
    $.post( "/games/load_provinces/",{"load_provincias":true}, 
        function( response ) {
            $("#provincia").empty();
	        $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');
	                
            var json = JSON.parse(response);
		    var provincias=json.provincias;
		    //alert(provincias);
		    //console.log(provincias);
		    //alert(provincias[0].id);
		    //alert(provincias[0].nombre);
    		
            if(provincias === 'error'){
                load_provincias_v2();
            }else{
                for (var i = 0; i < provincias.length; i++) { 
        		    $("#provincia").append("<option value='" + provincias[i].id + "'>" + provincias[i].nombre + "</option>");
    		    }
            }
    })
    .fail(function(response) {
        load_provincias_v2();
    });
}

function load_poblaciones_v2(prov) {
    $.get("/resources/provinciasypoblaciones.xml", function (xml) {
		$("#poblacion").empty();
	    $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');
			    
		$(xml).find('provincia[id=' + prov + ']').each(function(){
    		$(this).find('localidad').each(function(){
    			 $("#poblacion").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
    		});  
        });
	})
	.fail(function() {
        alert( "error load_poblaciones" );
    });
}

function load_poblaciones_v1(prov) { //provinciasypoblaciones.xml - xpath
    var datos = { idPoblac : prov  };
	
        $.post("/games/load_populations", datos, function(response) {	    
        var json = JSON.parse(response);
		var poblaciones=json.poblaciones;
		//alert(poblaciones);
		//console.log(poblaciones);
		//alert(poblaciones[0].poblacion);

		$("#poblacion").empty();
	    $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

        if(poblaciones === 'error'){
            load_poblaciones_v2(prov);
        }else{
            for (var i = 0; i < poblaciones.length; i++) { 
        		$("#poblacion").append("<option value='" + poblaciones[i].poblacion + "'>" + poblaciones[i].poblacion + "</option>");
    		}
        }
	})
	.fail(function() {
        load_poblaciones_v2(prov);
    });
}

