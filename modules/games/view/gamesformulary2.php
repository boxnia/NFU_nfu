<!--<link rel="stylesheet" href="<?php echo CONTACT_LIB_BOOT ?>bootstrap.min.css">
<link rel="stylesheet" href="<?php echo CONTACT_LIB_FONT ?>font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo CONTACT_LIB_FORM_STYLE ?>form-elements.css">
<link rel="stylesheet" href="<?php echo CONTACT_LIB_FORM_STYLE ?>style.css">-->
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>form-elements.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>style.css">  
<script type="text/javascript" src="<?php echo GAME_JS_PATH ?>games.js" ></script>
<link rel="stylesheet" href="<?php echo TIMEPIKER_GAME ?>jquery.timepicker.css">
<script type="text/javascript" src="<?php echo TIMEPIKER_GAME ?>jquery.timepicker.min.js" ></script>

    
    <!--<div class="col-md-6">-->
            <!--<div class="col-sm-6 col-sm-offset-3 form-box">-->
            <div class="col-sm-offset-3 col-sm-ofset3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Nueva Partida</h3>                        
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-futbol-o"></i>
                    </div>
                </div>
                <div class="form-bottom contact-form">                
                    <form id="form_contact" class="contact-form" name="form_contact">
                        
                        <div class="form-top-left">
                        <h3>Paso 1: Nombre y Deporte</h3>                        
                        </div>
                        <hr>
                        
                        <!--nombre-->
                        <div class="form-group">
                            <!--<div class="form-top-left">
                                <h4><label class="label label-default" for="contact-email">Asigna un Nombre</label></h4>
                            </div>-->                            
                            <input name="gamename" type="text" id="gamename" class="contact-email form-control" placeholder="Nombre de la patida">
                            <div id="e_gamename" style="color: #ff0000"></div>                           
                        </div>
                        <!--fin nombre-->
                        
                        <!--deporte-->
                        <!--<div class="form-top-left">
                            <h4><label class="label label-default" for="contact-email">Elige un deporte</label></h4>
                        </div>-->                           
                        <label class="selectpicker" for="deporte">Escoja un deporte</label>
                        <div class="form-group-contact">
                            <select  class="contact-subject form-control" name="sport" id="type_sport" readonly>
                                <optgroup label="deporte">
                                    <option value="futbol">Futbol</option>
                                    <option value="baloncesto">Baloncesto</option>
                                    <option value="voleibol">Voleibol</option>
                                    <option value="tenis">Tenis</option>
                                    <option value="padel">Padel</option>
                                </optgroup>
                            </select>
                        </div>
                        <!--fin deporte-->
                        
                        <div class="form-top-left">
                        <h3>Paso 2: ¿Dónde y Cuándo?</h3>                        
                        </div>
                        <hr>
                        
                        <!--Provincias-->
                            <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Seleccione una Provincia de destino</label></h4>
                            </div>-->
                            <label class="selectpicker" for="provincia">Seleccione la Provincia de la Instalación</label>
                            <select id="provincia" class="contact-province form-control">
                            </select>
                            <!--<span id="e_provincia" class="styerror"></span>-->
                        <!--fin Provincias-->
                        
                        <!--Poblaciones-->  
                            <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Seleccione una Poblacion de destino</label></h4>
                            </div>-->
                            <label class="selectpicker" for="poblacion">Seleccione la Poblacion de la Instalación</label>
                            <select id="poblacion" class="contact-city form-control"></select>
                            <br>
                            <!--<span id="e_poblacion" class="styerror"></span>-->
                        <!--fin Poblaciones-->   
                        
                        <!--instalacion-->                        
                        <div class="form-group">  
                            <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Instalacion</label></h4>
                            </div>-->                            
                            <input name="contact_install" type="text" id="contact_install" class="contact-email form-control" placeholder="Empieza a escribir el nombre de la instalación">            
                            <div id="e_contact_install" style="color: #ff0000"></div>
                        </div>
                        <div class="form-top-left">
                               <label class="selectpicker" for="anadir">No lo encuentras?</label> <button type="button" class="btn_games_form btn-success">Añadelo!</button>
                        </div>
                          
                        <!--fin instalacion-->
                        
                        
                        
                        
                        
                        <!--comentarios zona de encuentro-->                        
                        <div class="form-group">
                            <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Zona de encuentro</label></h4>
                            </div>-->                             
                            <textarea placeholder="Describe la zona de encuentro..." maxlength="255" rows="4" cols="50" class="form-control"></textarea>
                        </div>
                        <!--fin comentarios zona de encuentro-->      
                        
                        <!--dia de la partida-->
                        <div class="form-group">
                            <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Dia de la partida</label></h4>
                            </div>-->                             
                            <input id="day_game" type="text" name="days_game" placeholder="Dia de la partida" readonly class="contact-email form-control">
                            <div id="e_day_game" style="color: #ff0000"></div>
                            <div id="result"></div>                            
                        </div>
                        <!--fin dia de la partida-->                       

                        <!--hora-->
                        <div class="form-group"> 
                            <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Hora</label></h4>
                            </div>-->                             
                            <input name="times_start" type="text" id="time_start" class="contact-email form-control" placeholder="Hora de inicio">            
                            <div id="e_time_start" style="color: #ff0000"></div>
                        </div>
                        <!--fin hora-->
                        
                        <!--duracion-->  
                        <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Duración</label></h4>
                        </div>-->                         
                        <input name="durations" type="text" id="duration" class="contact-email form-control" placeholder="Duracion">                       
                        <div id="e_duration" style="color: #ff0000"></div>
                        <!--fin duracion-->
                        
                        <div class="form-top-left">
                        <h3>Paso 3: Configuracón</h3>                        
                        </div>
                        <hr>
                        
                        <!--numero de plazas-->                        
                        <div class="form-group">
                            <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Numero de plazas</label></h4>
                            </div>-->                            
                            <input name="places_number" type="text" id="places_number" class="contact-email form-control" placeholder="Numero de plazas">
                            <div id="e_places_number" style="color: #ff0000"></div>                             
                        </div>
                        <!--fin numero de plazas-->                        
                        
                        <!--tipo de entrada-->       
                        <!--<div class="form-top-left">
                               <h4><label class="label label-default" for="contact-email">Tipo de entrada</label></h4>
                        </div>-->                 
                        <label class="selectpicker" for="entrada">Seleccione el tipo de entrada al recinto</label>
                        <div class="form-group">
                            <div class="radio">
                                <label><input name="enter" type="radio" value="0" checked>Gratuita</label>
                            </div>
                            <div class="radio">
                                <label><input name="enter" type="radio" value="1">Pago</label>
                            </div>
                        </div>
                        <!--fin tipo de entrada-->
                                                                        
                        <!--catidad de entrada-->                           
                        <div class="form-group">
                            <input name="price" type="text" id="price_cash" class="contact-email form-control" placeholder="Precio de la entrada">
                            <div id="e_price_cash" style="color: #ff0000"></div>
                        </div>
                        <!--fin cantidad entrada-->
                                                                                                                                                                        
                        <!--enviar-->
                        <div class="form-group"> 
                            <button type="button" class="btn" name="boton" id="boton">Enviar Partida</button>
                            <!--<input type="button" class="btn" name="boton" id="boton" value="Enviar">-->                            
                        </div>
                        <!--fin enviar--> 
                        
                    </form>
                </div>
            </div>
        <!--</div>-->
    
    <!--</div>-->
        
        
        <!--<script type="text/javascript" src="<?php echo CONTACT_JS ?>jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="<?php echo CONTACT_JS_BOOT ?>bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo CONTACT_JS ?>jquery.backstretch.min.js"></script>
        <script type="text/javascript" src="<?php echo CONTACT_JS ?>retina-1.1.0.min.js"></script>
        <script type="text/javascript" src="<?php echo CONTACT_JS ?>scripts.js"></script>-->
                
                


