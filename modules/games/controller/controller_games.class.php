<?php
class controller_games {    
    /*estamos realizando la parte de coordenadas, ya funciona pero vamos a ver como registramos las partidas y crear la funcionalidad de filtrar las instalaciones*/
    function __construct(){        
        include(FUNCTIONS_GAME."functions.inc.php");/*es el modulo necesario*/        
    }

    function form_games() {
        /*cuidado con el hedader que estamos probando el de usuario, el que pertoca aqui es header_register*/
        require_once(VIEW_PATH_INC."header.html"); 
        require_once(VIEW_PATH_INC."menu.php");

        loadView('modules/games/view/', 'gamesformulary3.php');

        require_once(VIEW_PATH_INC."footer.html");
    }
    
    function results_games() {
        require_once(VIEW_PATH_INC."head.php"); 
        require_once(VIEW_PATH_INC."menu.php");

        loadView('modules/games/view/', 'gameok.php');

        require_once(VIEW_PATH_INC."footer.html");
    }



    /*function upload_file(){
        if ((isset($_POST["upload"])) && ($_POST["upload"] == true)) {
        $result_avatar = upload_files();
        $_SESSION['result_avatar'] = $result_avatar;        
    }
    }*/
    

    function up_games() {
        if (isset($_POST['data_games_JSON'])) {

            $jsondata = array();

            $usersJSON = json_decode($_POST["data_games_JSON"], true);

            

            $result = validate_game($usersJSON);
            

            

            /*if (empty($_SESSION['result_avatar'])) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => '../../../media/default-avatar.png');
               
            }*/

            //$result_avatar = $_SESSION['result_avatar'];

            /*if (($result['resultado']) && ($result_avatar['resultado'])) {*/
            if (($result['resultado'])) {
                
                $arrArgument = array(
                    'name' => strtoupper($result['datos']['name']),
                    'time' => $result['datos']['time'],
                    'duration' => $result['datos']['duration'],
                    'pricecash' => $result['datos']['pricecash'],
                    'places' => $result['datos']['places'],
                    'day' => strtoupper($result['datos']['day']),
                    'sport' => $result['datos']['sport'],
                    'provincia' => $result['datos']['provincia'],
                    'poblacion' => $result['datos']['poblacion']
                );
                
                $provincia = $arrArgument['provincia'];
                $poblacion = $arrArgument['poblacion'];
                $coor=coordenadas($provincia, $poblacion);
                echo debugPHP ($coor);                
                die();
                
                

                

                //$_SESSION['result_avatar'] = array();

                /* insert into BD begin */
                $arrValue = false;
                //set_error_handler('ErrorHandler');/*Nos esta creando errores, no podemos pasar de esta linea, nos crea error*/

                try {
                    
                    $arrValue = loadModel(MODEL_GAME, "game_model", "create_game", $arrArgument);                    

                } catch (Exception $ex) {

                    $arrValue = false;
                }
                
                /* insert into BD end */
                if ($arrValue) {
                    $mensaje = "Su Partida se ha regitrado correctamente";
                    $_SESSION['game'] = $arrArgument;
                    $_SESSION['msje'] = $mensaje;
                    
                    $callback = "/NFU/games/results_games";
                      
                    /*echo json_encode("linea 95 del controlador".$callback);
                    die();*/
                    
                    $jsondata["success"] = true;
                    $jsondata["redirect"] = $callback;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $mensaje = "No se ha podido realizar el alta de la partida. Porfavor intentelo mas tarde";
                }
            } else {
                $jsondata["success"] = false;
                $jsondata["error"] = $result['error'];
                //$jsondata["error_avatar"] = $result_avatar['error'];

                //Si la imagen de upload es correcta, pero los datos no
                $jsondata["success1"] = false;
                /*if ($result_avatar['resultado']) {
                    $jsondata["success1"] = true;
                    //$jsondata["img_avatar"] = $result_avatar['datos'];
                }*/

                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;
            }
            //no gastar header
        }
    }

    function delete(){
        /* delete */
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }
    

    function load_game(){         
        /* load */
        if (isset($_POST["load_game"]) && $_POST["load_game"] == true) {
            $jsondata = array();
            if (isset($_SESSION['game'])) {
                //echo debug($_SESSION['user']);
                $jsondata['game'] = $_SESSION['game'];
            }
            if (isset($_SESSION['msje'])) {
                //echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }
            
            if($jsondata){
                close_session(); /*en utils/utils.inc.php*/
                echo json_encode($jsondata);
                exit;
            }else{
                close_session(); /*en utils/utils.inc.php*/
                showErrorPage(1, "", 'HTTP/1.0 404 Not Found', 404);
            }            
        }
    }

    function load_data_game(){
        /* load data */
        if ((isset($_POST["load_data_game"])) && ($_POST["load_data_game"] == true)) {
            $jsondata = array();

            if (isset($_SESSION['game'])) {
                $jsondata["prod"] = $_SESSION['game'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["prod"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }
    

    function load_countries(){
        /* load countries */
        if ((isset($_POST["load_pais"])) && ($_POST["load_pais"] == true)) {
            $json = array();

            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';
            try {
                
                $json = loadModel(MODEL_FORM, "prodModel", "obtain_countries", $url);
            } catch (Exception $ex) {
                $json = array();
            }


            if ($json) {
                echo $json;
                exit;
            } else {
                $json = "error";
                echo $json;
                exit;
            }
        }
    }
   
    function load_provinces(){
        /* load provinces */
        if ((isset($_POST["load_provincias"])) && ($_POST["load_provincias"] == true)) {
            $jsondata = array();
            $json = array();

            try {
                
                $json = loadModel(MODEL_GAME, "game_model", "obtain_provinces");
            } catch (Exception $ex) {
                $json = array();
            }


            if ($json) {
                $jsondata["provincias"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["provincias"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }  
    }
    

    function load_populations(){
        /* load populations */
        if (isset($_POST['idPoblac'])) {
            $jsondata = array();
            $json = array();

            try {
                
                $json = loadModel(MODEL_GAME, "game_model", "obtain_populations", $_POST['idPoblac']);
            } catch (Exception $ex) {
                $json = array();
            }


            if ($json) {
                $jsondata["poblaciones"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["poblaciones"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        } 
    }    
    
    /*function coordenadas($provincia,$poblacion){

    $direccion = $provincia.", ".$poblacion;
    /*nom hemos kedado mirando la funcionalidad de esta funcion
    $direccion_google = 'Calle, Población, Provincia / Estado, País';
    $resultado = file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s', urlencode($direccion_google)));
    $resultado = json_decode($resultado, TRUE);

    $lat = $resultado['results'][0]['geometry']['location']['lat'];
    $lng = $resultado['results'][0]['geometry']['location']['lng'];

    $coor = Array($lat, $lng); 
    echo debugPHP ($coor);                
    die();
    return $coor;
    }*/
}

    