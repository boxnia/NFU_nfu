<?php

class game_dao {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_game_DAO($db, $arrArgument) {
        
        
        $id = null;
        $name = $arrArgument['name'];
        $ubicacion = "ontinyent";
        $time = $arrArgument['time'];
        $duration = $arrArgument['duration'];
        $pricecash = $arrArgument['pricecash'];
        $places = $arrArgument['places'];
        $day = $arrArgument['day'];
        $sport = $arrArgument['sport'];

        
        
        /*Asi es como quedara la base de datos con las poblaciones, no lo hemos hecho todavia porque estamos en pruebas*/
        $sql = "INSERT INTO game (id, nombre, ubicacion, deporte,"
                . " hora, duracion, inscripcion, plazas, dia)"
                . " VALUES ('$id', '$name', '$ubicacion',"
                . " '$sport', '$time', '$duration', '$pricecash', '$places', '$day')";             

        return $db->ejecutar($sql);
    }

    /*public function obtain_countries_DAO($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $file_contents = curl_exec($ch);
        curl_close($ch);

        return ($file_contents) ? $file_contents : FALSE;
    }*/

    public function obtain_provinces_DAO() {
        $json = array();
        $tmp = array();

        /*$provincias = simplexml_load_file("../../../../resources/provinciasypoblaciones.xml");*/
        $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT']."/NFU/resources/provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
        for ($i = 0; $i < count($result); $i+=2) {
            $e = $i + 1;
            $provincia = $result[$e];

            $tmp = array(
                'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }

    public function obtain_populations_DAO($arrArgument) {
        $json = array();
        $tmp = array();

        $filter = (string) $arrArgument;
        /*$xml = simplexml_load_file('../../../../resources/provinciasypoblaciones.xml');*/
        $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT']."/NFU/resources/provinciasypoblaciones.xml");
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }

}
