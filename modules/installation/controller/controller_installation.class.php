<?php


class controller_installation {

//controller_services será cargado tanto desde lis_services.js como por index.php

    public function __construct() {

        include(UTILS_INSTALLATION . "utils.inc.php");
       
       // include LOG_DIR;
       /* include(UTILS . "filters.inc.php");
        include(UTILS . "common.inc.php");

        include(UTILS . "response_code.inc.php");*/
        $_SESSION['users'][0];
       
        $_SESSION['module'] = "installation";
    }

    public function list_installation() {
        
        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/installation/view/', 'list_installations.php');

        require_once(VIEW_PATH_INC . "footer.html");
       
    }

    /*FUNCIONES CREAR INSTALACION*/
    /*estamos modificando esta funciones y las que hay a partir de la linea 321*/
   /* public function form_install() {
        require_once(VIEW_PATH_INC."header_register.php"); 
        require_once(VIEW_PATH_INC."menu.php");

        loadView('modules/installation/view/', 'formulario.php');

        require_once(VIEW_PATH_INC."footer.html");
    }*/
    /*
    public function results_install() {
        require_once(VIEW_PATH_INC."header_register.php"); 
        require_once(VIEW_PATH_INC."menu.php");

        loadView('modules/installation/view/', 'registroinstall.php');

        require_once(VIEW_PATH_INC."footer.html");
    }
    /*FIN FUNCIONES CREAR INSTALACION*/
    
    public function autocomplete_installation() {
      
        if ((isset($_POST["autocomplete"])) && ($_POST["autocomplete"] === "true")) {
            set_error_handler('ErrorHandler');
             
            try {
//loadmodel
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/installation/model/model/';
                $results = loadModel(MODEL_INSTALLATION, "installation_model", "order_install_nombre");


//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($results) {
                $jsondata["nom_install"] = $results;
                echo json_encode($jsondata);
                exit;
            } else {
//if($nom_installationos){ //que lance error si no hay installationos
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function nom_installation() {
       
        if (($_POST["nom_install"])) {
//filtrar $_GET["nom_installation"]
            $result = filter_string($_POST["nom_install"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_5/modules/installation/model/model/';
                $installation = loadModel(MODEL_INSTALLATION, "installation_model", "filter_install_nombre", $criteria);

//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($installation) {

                $jsondata["install_autocomplete"] = $installation;
                echo json_encode($jsondata);
                exit;
            } else {
//if($installationo){{ //que lance error si no existe el installationo
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function count_installation() {

        if (($_POST["count_install"])) {
//filtrar $_GET["count_installation"]
            $result = filter_string($_POST["count_install"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_5/modules/installation/model/model/';
                $resul = loadModel(MODEL_INSTALLATION, "installation_model", "count_install_criteria", $criteria);
                $total_rows = $resul[0]["total_filter"];
//throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($total_rows) {

                $jsondata["num_install"] = $total_rows[0];
                echo json_encode($jsondata);
                exit;
            } else {
//if($total_rows){ //que lance error si no existe el installationo
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function num_pages_installation() {
        if ((isset($_POST["num_pages"])) && ($_POST["num_pages"] == true)) {
           
            if (isset($_POST["keyword"])) {
                
                $result = filter_string($_POST["keyword"]);

                if ($result['resultado']) {
                    $criteria = $result['datos'];
                } else {
                    $criteria = '';
                }
            } else {
                $criteria = '';
            }

            $item_per_page = 3;
            $pages = 0;
            set_error_handler('ErrorHandler');

            try {
//throw new Exception();
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_5/modules/installation/model/model/';
                $results = loadModel(MODEL_INSTALLATION, "installation_model", "count_install_criteria", $criteria);

                $get_total_rows = $results[0]["total_filter"]; //total records	
                $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();
 

            if ($get_total_rows) {
                $jsondata["pages"] = $pages;
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function view_error_true() {
        if ((isset($_POST["view_error"])) && ($_POST["view_error"] === "true")) {

            showErrorPage(0, " 503 BD Unavailable");
        }
    }

    public function view_error_false() {

        if ((isset($_POST["view_error"])) && ($_POST["view_error"] === "false")) {

            showErrorPage(3, "OOPS! - Could not Find it");
        }
    }

    public function idInstallation() {

        if ($_POST["idInstall"]) {


            $result = filter_num_int($_POST["idInstall"]);

            if ($result['resultado']) {

                $id = $result['datos'];
            } else {
                $id = 1;
            }
            set_error_handler('ErrorHandler');
            try {
// throw new Exception(); //para probar que entre en el catch
//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_5/modules/installation/model/model/';
                $install = loadModel(MODEL_INSTALLATION, "installation_model", "details_install", $id);
            } catch (Exception $e) {

                showErrorPage(2, "", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($install) {

//require_once 'modules/services/view/details_services.php';
//loadView('modules/installation/view/', 'details_installations.php', $install[0]);
                $jsondata["install"] = $install[0];
                echo json_encode($jsondata);
                exit;
            } else {

                showErrorPage(2, "", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

   public function obtain_installation() {

        $item_per_page = 3;


//filtrar $_POST["page_num"]
        if (isset($_POST["page_num"])) {
            
            $result = filter_num_int($_POST["page_num"]);
            if ($result['resultado']) {
                $page_number = $result['datos'];
            }
        } else {
            $page_number = 1;
        }

//filtrar $_GET["keyword"]
        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }
        } else {
            $criteria = '';
        }

        if (isset($_POST["keyword"])) {

            $result = filter_string($_POST["keyword"]);

            if ($result['resultado']) {
                $criteria1 = $result['datos'];
            } else {
                $criteria1 = '';
            }
        } else {
            $criteria1 = '';
        }


        if (isset($_POST["keyword"])) {

            $criteria = $criteria1;
        }




        $position = (($page_number - 1) * $item_per_page);


// $path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_5/modules/installation/model/model/';
        $arrArgument = array(
            'position' => $position,
            'item' => $item_per_page,
            'criteria' => $criteria,
        );
        set_error_handler('ErrorHandler');
        try {
//throw new Exception();
            $arrValue = loadModel(MODEL_INSTALLATION, "installation_model", "order_install_criteria", $arrArgument);
        } catch (Exception $e) {
//header('HTTP/1.0 503 Service Unavailable', true, 503); no! tenemos que pintar html
            showErrorPage(0, " 503 BD Unavailable");
        }
        restore_error_handler();

        if ($arrValue) {

            paint_template_installation($arrValue);
        } else {
            showErrorPage(0, " 404 NO SPORTS FACILITIES");
        }
    }

    /*FUNCIONES CREAR INSTALACION*/

   /* public function upload_install(){
        if ((isset($_POST["upload"])) && ($_POST["upload"] == true)) {
        $result_avatar = upload_files();
        $_SESSION['result_avatar'] = $result_avatar;
        //echo debug($_SESSION['result_avatar']); //se mostraría en alert(response); de dropzone.js, a tener en cuenta por si falla.
    }
    }
    
    public function up_install() {
        if (isset($_POST['data_install_JSON'])) {

            $jsondata = array();

            $usersJSON = json_decode($_POST["data_install_JSON"], true);

            //gastar header malament                   

            $result = validate_install($usersJSON);

            //echo debug($result); //se mostraría en alert(response); de $.post('pages/controller_users.pp ...

            if (empty($_SESSION['result_avatar'])) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => '../../../media/default-avatar.png');
               
            }

            $result_avatar = $_SESSION['result_avatar'];

            if (($result['resultado']) && ($result_avatar['resultado'])) {
                $arrArgument = array(
                    'tipoproducto' => strtoupper($result['datos']['tipoproducto']),
                    'nombreproducto' => $result['datos']['nombreproducto'],
                    'codigo' => $result['datos']['codigo'],
                    'date_exit' => strtoupper($result['datos']['date_exit']),
                    'arrival_date' => strtoupper($result['datos']['arrival_date']),
                    'color' => $result['datos']['color'],
                    'pais' => strtoupper($result['datos']['pais']),
                    'provincia' => strtoupper($result['datos']['provincia']),
                    'poblacion' => strtoupper($result['datos']['poblacion']),
                    'avatar' => $result_avatar['datos']
                );



                $_SESSION['result_avatar'] = array();

                /* insert into BD begin */

       /*         $arrValue = false;
                set_error_handler('ErrorHandler');/*Nos esta creando errores, no podemos pasar de esta linea, nos crea error*/

         /*       try {
                    /*Esta es la manera antigua de llamar a las funciones de load model*/
                    /*$path_model = $_SERVER['DOCUMENT_ROOT'] . '/plantillaweb/modules/formulary/model/model/';
                    $arrValue = loadModel($path_model, "prodModel", "create_prod", $arrArgument);*/
                    /*Esta es la manera nueva de llamar a las funciones de load model*/
       /*             $arrValue = loadModel(MODEL_FORM, "prodmodel", "create_prod", $arrArgument);

                } catch (Exception $ex) {

                    $arrValue = false;
                }
                restore_error_handler();
                //hemos debugueado hasta la clase de Db sin problemas, el error podria ser sintaxico              
                /* insert into BD end */
          /*      if ($arrValue) {
                    $mensaje = "Su registro se ha efectuado correctamente, para finalizar compruebe que ha recibido un correo de validacion y siga sus instrucciones";
                    $_SESSION['install'] = $arrArgument;
                    $_SESSION['msje'] = $mensaje;
                    
                    $callback = "/plantillaweb/formulary/results_install";
                    
                    $jsondata["success"] = true;
                    $jsondata["redirect"] = $callback;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $mensaje = "No se ha podido realizar su alta. Intentelo mas tarde";
                }
            } else {
                $jsondata["success"] = false;
                $jsondata["error"] = $result['error'];
                $jsondata["error_avatar"] = $result_avatar['error'];

                //Si la imagen de upload es correcta, pero los datos no
                $jsondata["success1"] = false;
                if ($result_avatar['resultado']) {
                    $jsondata["success1"] = true;
                    $jsondata["img_avatar"] = $result_avatar['datos'];
                }

                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;
            }
            //no gastar header
        }
    }
    
    public function delete(){
        /* delete */
      /*  if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }
    
    
    public function load(){         
        /* load */
    /*    if (isset($_POST["load"]) && $_POST["load"] == true) {
            $jsondata = array();
            if (isset($_SESSION['install'])) {
                //echo debug($_SESSION['user']);
                $jsondata['install'] = $_SESSION['install'];
            }
            if (isset($_SESSION['msje'])) {
                //echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }
            
            if($jsondata){
                close_session(); /*en utils/utils.inc.php*/
          /*      echo json_encode($jsondata);
                exit;
            }else{
                close_session(); /*en utils/utils.inc.php*/
          /*      showErrorPage(1, "", 'HTTP/1.0 404 Not Found', 404);
            }            
        }
    }
    
    public function load_data_install(){
        /* load data */
     /*   if ((isset($_POST["load_data_install"])) && ($_POST["load_data_install"] == true)) {
            $jsondata = array();

            if (isset($_SESSION['install'])) {
                $jsondata["install"] = $_SESSION['install'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["install"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }
    /*FIN FUNCIONES CREAR INSTALACION*/
    
}


