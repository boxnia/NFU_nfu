<?php

function paint_template_error($message) {
    $log = log::getInstance();
    $log->add_log_general("error paint_template_error", "installation", "response " . http_response_code()); //$text, $controller, $function
    $log->add_log_user("error paint_template_error", "", "installation", "response " . http_response_code()); //$msg, $username = "", $controller, $function
    // $css = file_get_contents('/view/css/media.css');
    print("<link rel='stylesheet' href='" . CSS_PATH . "media.css'/>");
    print ("<div class='uno'> \n");
    print ("<img src='media/error.png' alt='troopers needs cofee too!'/>\n");
    print ("<p class='error1'> \n");
    if (isset($message) && !empty($message)) {
        print( '<h1>ERROR  - ' . $message . '</h1>');
    }
   

    print ("</p><p class='funny'>No Coffee, No Workee!</p> \n");
    print ("<p><a id='error1' href='/main/begin' title='Back on track trooper!'>Volver a inicio</a></p> \n");

    print ("</div> \n");
}

function paint_template_installation($resultado) {
    print ("<script type='text/javascript' src='" . INSTALLATION_JS_PATH . "modal_installation.js' ></script> \n");
    print ("<section> \n");
    print ("<div class='container'> \n");
    print ("<div class='row text-center pad-row'> \n");

    foreach ($resultado as $install) {
        print ("<div class='col-md-4  col-sm-4'> \n");
        print ("<div class='thumbnail'>");
        print ("<img src='" . MEDIA_PATH .$install['avatar'] . "' alt=''> \n");
        //print ("<img src='". $productos['img'] . "' alt='product' height='70' width='70'> \n");
        print ("<div class='caption'>");
        print ("<h3>" . $install['nombre'] . "</h3> \n");
        print ("<p>" . $install['ubicacion'] . "</p> \n");
        print ("<div class='ratings'>");
        print("<p><span class='glyphicon'><label>Valoracion:</label></span></p>");
        print ("<img class='polaroids' src='" . MEDIA_PATH . $install['valoracion'] . "' alt='' class='polaroids'> \n");

        echo "<div id='" . $install['id'] . "' class='btn2'>Más Info</div>";


        print ("</div> \n");
        print ("</div> \n");
        print ("</div> \n");
        print ("</div> \n");
    }
    print ("</div> \n");
    print ("</div> \n");
    print ("</section> \n");
}

function paint_template_search($message) {
    $log = log::getInstance();
    $log->add_log_general("error paint_template_search", "installation", "response " . http_response_code()); //$text, $controller, $function
    $log->add_log_user("error paint_template_search", "", "installation", "response " . http_response_code()); //$msg, $username = "", $controller, $function
    print("<link rel='stylesheet' href='" . INSTALLATION_CSS_PATH . "installation.css'/>");
    print ("<section> \n");
    print (" <div class='wrap'>\n");
 print (" <center>\n");

    print ("<h2>" . $message . "</h2> \n");
    print ("<img src=" . MEDIA_PATH . "circulo.png height='100' width='100' > \n ");
    print ("</br> \n");
    print ("</br> \n");
    print ("<div class='sub' \n");
    print ("<p><a href='/installation/list_installation'>Volver a instalaciones </a></p> \n");
     print (" </center>\n");
    print ("</div> \n");

  print ("</br> \n");

    print ("</div> \n");
    print ("</section> \n");
}
