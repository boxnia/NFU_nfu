
<?php

class controller_contact {

    public function __construct() {
        $_SESSION['modules'] = "contact";
    }

    public function view_contact() {
        require_once(VIEW_PATH_INC."header.html"); 
        require_once(VIEW_PATH_INC."menu.php");

        loadView(CONTACT_VIEW_PATH, 'contact3.php');

        require_once(VIEW_PATH_INC."footer.html");
    }

    public function check_contact() {
        
        if ($_POST['token'] === "contact_form") {
            
            //////////////// Envio del correo al usuario
            $arrArgument = array(
                'type' => 'contact',
                'token' => '',
                'inputName' => $_POST['inputName'],
                'inputEmail' => $_POST['inputEmail'],
                'inputSubject' => $_POST['inputSubject'],
                'inputMessage' => $_POST['inputMessage']
            );
            
            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {
                    /*entra en la funcion envir email e ir a la linea 52*/
                    /*echo "estamos dentro del if form_contact del controlador contract y dentro del if enviaremail";            
                    die();*/
                    echo "<div class='alert alert-success'>Su mensaje a sido enviado</div>";
                } else {
                    echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                }
            } catch (Exception $e) {
                echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
            }
            restore_error_handler();


            //////////////// Envio del correo al admin de la ap web
            $arrArgument = array(
                'type' => 'admin',
                'token' => '',
                'inputName' => $_POST['inputName'],
                'inputEmail' => $_POST['inputEmail'],
                'inputSubject' => $_POST['inputSubject'],
                'inputMessage' => $_POST['inputMessage']
            );            
            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {
                    /*mirr porque esta petando en la funcion enviar email, ya que los parametros pasados son los correctos*/
                    /*echo "<div class='alert alert-success'>Su mensaje a sido enviado</div>";*/
                } else {
                    echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                }
            } catch (Exception $e) {
                echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
            }
            
            restore_error_handler();
        } else {
            echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
        }
    }

}
