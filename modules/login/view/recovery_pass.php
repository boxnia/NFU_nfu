<script type="text/javascript" src="<?php echo LOGIN_JS_PATH ?>recovery_pass.js" ></script>
<!--<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>form-elements.css">-->
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>style.css">
<!--<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>font-awesome/css/font-awesome.min.css">-->
<link rel="stylesheet" type="text/css" href="<?php echo LOGIN_CSS_PATH ?>login.css">

<div class="row">
    
    <div class="distance_recovery col-md-6 col-md-offset-3">
        <div class="distance_recovery">
            <h1 class="color_text_recovery"><p>Introduzca su nueva contraseña</p></h1>
            <div>                
                <input type="password" id="recovery_pass" class="form-control" placeholder="Nueva contraseña">                
            </div>
            <div>
                <input type="password" id="recovery_pass2" class="form-control" placeholder="Repita contraseña">                
            </div>
            <div>
                <input type="button" class="btn" name="boton" id="boton_recovery_pass" value="Enviar">
            </div>
        </div>
    </div>
</div>
