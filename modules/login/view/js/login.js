$(document).ready(function () {
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var nombrereg = /^([a-z ñáéíóú]{2,60})$/i;
    var codigoreg = /^([0-9]{5,10})*$/;
    var numeroreg = /^([0-9])*$/;
    var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;

    $.post("/login/load_users", {load: true},
    function (response) {

        if (response.users === "") {

            $("#nombre").val('');
            $("#password").val('');
            $("#password2").val('');
            $("#email").val('');
            $("#fecha_nac").val('');




        } else {

            $("#nombre").attr("value", response.users.nombre);
            $("#email").attr("value", response.users.email);
            $("#fecha_nac").attr("value", response.users.fecha_nac);

        }
    }, "json");


    $("#fecha_nac").datepicker({
        changeMonth: true,
        changeYear: true
    });


/////////////////////////alta_user
    $('#boton').click(function () {
        validate_users();

    });
/////////////////////////////confirm_password_recovery
    $('#recovery').click(function () {
        view_recovery();

    });
    /////////////////////////////email_recovery_password  
    $('#boton_recovery').click(function () {
        // alert("hola");
        recovery_pass_email();

    });

/////////////////////////facebook
    $('#facebook').click(function () {
        login();

    });
    
    /////////////////////////twiter
    $('#twitter').click(function () {
        login_twitter();

    });


    window.fbAsyncInit = function () {

        FB.init({
            appId: '1657918601126669', // App ID
            channelUrl: 'https://connect.facebook.net/en_US/all.js', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        FB.Event.subscribe('auth.authResponseChange', function (response) {
            if (response.status === 'connected') {
                console.log("Connected to Facebook");
                //SUCCESS
            } else if (response.status === 'not_authorized') {
                console.log("Failed to Connect");
                //FAILED
            } else {
                console.log("Logged Out");
                //UNKNOWN ERROR
            }
        });
    };


    $("#nombre,#email,#password, #password2").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#sed_email_recovery").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });


    $("#nombre").keyup(function () {

        if ($(this).val().length >= 2 && nombrereg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });


    $("#email").keyup(function () {
        if ($(this).val() != "" && emailreg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });


    $("#password").keyup(function () {
        if ($(this).val() != "" && passwordre.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });


    $("#password2").keyup(function () {
        if ($(this).val() != "" && passwordre.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

});

////////////////////////////////validate_user+ alta_user
function validate_users() {
    var result = true;
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var nombrereg = /^([a-z ñáéíóú]{2,60})$/i;
    var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;

    $(".error").remove();

    if ($("#nombre").val() == "" || !nombrereg.test($("#nombre").val()) || $("#nombre").val() == "Introduzca su nombre") {
        $("#nombre").focus().after("<span class='error'>Ingrese su nombre</span>");
        result = false;
    }
    else if ($("#nombre").val().length < 2) {
        $("#nombre").focus().after("<span class='error'>Mínimo 2 carácteres para el nombre</span>");
        result = false;
    } else if ($("#password").val() == "" || !passwordre.test($("#password").val()) || $("#password").val() == "Introduzca su password") {
        $("#password").focus().after("<span class='error'>Password debe tener al menos 6 caracteres,una letra mayúscula, un carácter numerico</span>");
        result = false;
    } else if ($("#password2").val() == "" || !passwordre.test($("#password2").val()) || $("#password2").val() == "Introduzca su password" || $("#password").val() !== $("#password2").val()) {
        $("#password2").focus().after("<span class='error'>Los 2 password deben coincidir</span>");
        result = false;
    } else if ($("#email").val() == "" || !emailreg.test($("#email").val()) || $("#email").val() == "Introduzca su email") {
        $("#email").focus().after("<span class='error'>Ingrese un email correcto</span>");
        result = false;
    }
    else {

        result = true;
    }

    var nombre = $("#nombre").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var password1 = $("#password2").val();
    var fecha_nac = document.getElementById('fecha_nac').value;


    var fecha_registro = getFormattedDate(new Date());
//alert(fecha_registro);

    function getFormattedDate(fecha_registro) {
        var year = fecha_registro.getFullYear();
        var month = (1 + fecha_registro.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = fecha_registro.getDate().toString();
        day = day.length > 1 ? day : '0' + day;

        return day + '/' + month + '/' + year;
    }

    if (result) {

        var data = {
            "nombre": nombre,
            "email": email,
            "password": password,
            "password2": password1,
            "fecha_nac": fecha_nac,
            "fecha_registro": fecha_registro,
           
        }

        var data_users_JSON = JSON.stringify(data);

        $.post("/login/alta_users",
                {alta_users_json: data_users_JSON},
        function (response) {
           console.log(response)

            if (response.success) {

                $('.ajaxLoader').fadeIn("fast");

                var dataString = $("#formulario").serialize();
                //console.log(dataString);
                $.ajax({
                    type: "POST",
                    url: "/login/process_contact",
                    data: dataString,
                    success: function (dataString) {
                        console.log(response.succes)
                        paint(dataString);
                    }
                })
                        .fail(function () {
                            paint("<div class='alert alert-error'>Server error. Try later...</div>");
                        });

                return false;
                //window.location.href = response.redirect;

            }
            ///// per a debuguejar loadmodel ////////
            //alert(response);  
            //.log(response);
            //////////////////////////////////////////
            //}); //para debuguear
        }, "json")
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert( "error" );

                    //alert(xhr.status);
                    //alert(textStatus);
                    // alert(errorThrown);

                    if (xhr.status === 0) {
                        console.log('Not connect: Verify Network.');
                    } else if (xhr.status == 404) {
                        console.log('Requested page not found [404]');
                    } else if (xhr.status == 500) {
                        console.log('Internal Server Error [500].');
                    } else if (textStatus === 'parsererror') {
                        console.log('Requested JSON parse failed.');
                    } else if (textStatus === 'timeout') {
                        console.log('Time out error.');
                    } else if (textStatus === 'abort') {
                        console.log('Ajax request aborted.');
                    } else {
                        console.log('Uncaught Error: ' + xhr.responseText);
                    }
                    //alert(xhr.responseJSON.error.nombre );
                    xhr.responseJSON = JSON.parse(xhr.responseText);

                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.nombre !== undefined && xhr.responseJSON.error.nombre !== null) {
                            //   $("#e_nombre").text(xhr.responseJSON.error.nombre);
                            $("#nombre").focus().after("<span class='error'>" + xhr.responseJSON.error.nombre + "</span>");
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.email !== undefined && xhr.responseJSON.error.email !== null) {
                            //$("#e_email").text(xhr.responseJSON.error.email);
                            $("#email").focus().after("<span class='error'>" + xhr.responseJSON.error.email + "</span>");
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.password !== undefined && xhr.responseJSON.error.password !== null) {
                            //$("#e_password").text(xhr.responseJSON.error.password);
                            $("#password").focus().after("<span class='error'>" + xhr.responseJSON.error.password + "</span>");
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.password2 !== undefined && xhr.responseJSON.error.password2 !== null) {
                            //$("#e_password2").text(xhr.responseJSON.error.password2);

                            $("#password2").focus().after("<span class='error'>" + xhr.responseJSON.error.password2 + "</span>");
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.fecha_nac !== undefined && xhr.responseJSON.error.fecha_nac !== null) {
                            // $("#e_date_birthday").text(xhr.responseJSON.error.fecha_nac);
                            $("#fecha_nac").focus().after("<span class='error'>" + xhr.responseJSON.error.fecha_nac + "</span>");

                        }
                    }
                });






    }

}


function view_recovery() {

    /*$.ajax({
     type: "POST",
     url: "/login/view_recovery",        
     })    */
    window.location.href = "/login/view_recovery";

}


function recovery_pass_email() {
    var result = true;
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

    $(".error").remove();
    if ($("#sed_email_recovery").val() == "" || !emailreg.test($("#sed_email_recovery").val()) || $("#sed_email_recovery").val() == "Introduzca su email o el email es incorrecto") {
        $("#sed_email_recovery").focus().after("<div class='error'><center>Ingrese un email correcto</center></div>");
        result = false;
    }
    else {

        result = true;
    }

    var email = $("#sed_email_recovery").val();

    if (result) {

        var data = {
            "email": email,
        }

        var recovery_email_JSON = JSON.stringify(data);
        /*alert(recovery_email_JSON);*/


        $.post("/login/email_recovery", {recovery_email_JSON: recovery_email_JSON},
        function (response) {
            console.log(response);
            if (!response.success) {
                /*alert(response.mensaje);
                 swal(response.mensaje);
                 /*$("#sed_email_recovery").focus().after("<div class='error'>"+response.mensaje+"</div>");*/
                /*paint("<div class='alert alert-error'>"+response.mensaje+"</div>");*/
                /*setTimeout(function(){ window.location.href = response.redirect; }, 3000);   */

                $("#sed_email_recovery").focus().after("<div class='error'><center>" + response.mensaje + "</center></div>");
            } else {
                /*$("#sed_email_recovery").focus().after("<div class='error'><center>"+response.mensaje+"</center></div>");*/
                /*$("#sed_email_recovery").focus().after("<div class='error'>El email introducino no existe o hay algun problema en el servidor</div>");*/
                //alert(response.mensaje);
                swal(response.mensaje);
                setTimeout(function () {
                    window.location.href = response.redirect;
                }, 3000);
            }
        }, "json")/*.fail(function () {
         /*paint("<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>");*/
                /*$("#sed_email_recovery").focus().after("<div class='error'>El email introducino no existe o hay algun problema en el servidor</div>");
                 $("#sed_email_recovery").focus().after("<div class='error'>"+response.mensaje+"</div>");
                 })*/;
    }









}


function paint(dataString) {


    // hide ajax loader icon
    $('.ajaxLoader').fadeOut("fast");
    swal(dataString);

    $("#nombre").val('');

    $("#password").val('');
    $("#password2").val('');
    $("#email").val('');
    $("#fecha_nac").val('');
}







function login() {
    FB.login(function (response) {
        if (response.authResponse) {
            getUserInfo();
        } else {
            console.log('User cancelled login or did not fully authorize');
        }
    }, {scope: 'email,user_photos,user_videos'});
}

function getUserInfo() {

    FB.api('/me', {fields: 'name, id,email, picture'}, function (response) {

  
        var data= {
            "nombre": response.name,
            "email": response.email,
            "picture": response.picture.data.url,
            "id":response.id,
        }
    var date_facebook = JSON.stringify(data);
   
  
       //alert(date_facebook);
        $.post("/login/social_facebook",
                {date_facebook: date_facebook},
        function (response) {
            console.log(response)

            if (response.success) {


            window.location.href = response.redirect;

            }else{
                
                
                swal(response.error);
            }
            ///// per a debuguejar loadmodel ////////
            //alert(response);  
            //.log(response);
            //////////////////////////////////////////
            //}); //para debuguear
        }, "json")
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert( "error" );

                    //alert(xhr.status);
                    //alert(textStatus);
                    // alert(errorThrown);

                   
                });

    });
}

function login_twitter(){
    /*logout twiter con logout normal, es decir, con sesion destroy()*/
    $.post("/login/social_twiter",
    /*$.post("http://cors.io/?u=http://51.254.97.198/login/social_twiter",{},*/
        function (response) {
           console.log(response.redirect);

            if (!response.success) {  
                swal(response.mensaje);               
            }else{
                window.location.href = response.redirect;
            }
            
        }, "json");
    
   
}


function Logout() {
    FB.logout(function () {
        document.location.reload();
    });
}

// Load the SDK asynchronously
(function (d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));