
$(document).ready(function () {
    var nombrereg = /^([a-z ñáéíóú]{2,60})$/i;
    var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;

    //$("#codigo").val('');
    $("#nombre_singin").val('');
    $("#password_singin").val('');
  


    $("#fecha_nac").datepicker({
        changeMonth: true,
        changeYear: true
    });



    $('#boton_loguear').click(function () {
        // alert("hola");
        validate_login();

    });

 

    $("#nombre_singin,#password_singin").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });






    $("#nombre_singin").keyup(function () {

        if ($(this).val().length >= 2 && nombrereg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#password_singin").keyup(function () {
        if ($(this).val() != "" && passwordre.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });


});


function validate_login() {
    var result = true;
    var nombrereg = /^([a-z ñáéíóú]{2,60})$/i;
    var passwordre = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;


    $(".error").remove();

    if ($("#nombre_singin").val() == "" || !nombrereg.test($("#nombre_singin").val()) || $("#nombre_singin").val() == "Username...") {
        $("#nombre_singin").focus().after("<span class='error'>Ingrese su nombre</span>");
        result = false;
    }
    else if ($("#nombre_singin").val().length < 2) {
        $("#nombre_singin").focus().after("<span class='error'>Mínimo 2 carácteres para el nombre</span>");
        result = false;
    } else if ($("#password_singin").val() == "" || !passwordre.test($("#password_singin").val()) || $("#password_singin").val() === "Password...") {
        $("#password_singin").focus().after("<span class='error'>Password debe tener al menos 6 caracteres,una letra mayúscula, un carácter numerico</span>");
        result = false;
    } 
    else {

        result = true;
    }

    var nombre = $("#nombre_singin").val();
    var password = $("#password_singin").val();
  

    if (result) {

        var data = {
            "nombre": nombre,     
            "password": password,
           
        }

        var data_users_JSON = JSON.stringify(data);


      //  alert(data_users_JSON);


        $.post("/login/singin",
                {login: data_users_JSON},
        function (response) {
            console.log(response)

            if (response.success) {
                //console.log(response.succes);
                //alert("logueado");
             
              window.location.href = response.redirect;

            }
            ///// per a debuguejar loadmodel ////////
            //alert(response);  
            //.log(response);
            //////////////////////////////////////////
            //}); //para debuguear
        }, "json")
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert( "error" );

                    //alert(xhr.status);
                    //alert(textStatus);
                    // alert(errorThrown);

                    if (xhr.status === 0) {
                        console.log('Not connect: Verify Network.');
                    } else if (xhr.status == 404) {
                        console.log('Requested page not found [404]');
                    } else if (xhr.status == 500) {
                        console.log('Internal Server Error [500].');
                    } else if (textStatus === 'parsererror') {
                        console.log('Requested JSON parse failed.');
                    } else if (textStatus === 'timeout') {
                        console.log('Time out error.');
                    } else if (textStatus === 'abort') {
                        console.log('Ajax request aborted.');
                    } else {
                        console.log('Uncaught Error: ' + xhr.responseText);
                    }
                
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                        
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.nombre !== undefined && xhr.responseJSON.error.nombre !== null) {
                            //   $("#e_nombre").text(xhr.responseJSON.error.nombre);
                            $("#nombre_singin").focus().after("<span class='error'>" + xhr.responseJSON.error.nombre + "</span>");
                        }
                    }
               
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.password !== undefined && xhr.responseJSON.error.password !== null) {
                            //$("#e_password").text(xhr.responseJSON.error.password);
                            $("#password_singin").focus().after("<span class='error'>" + xhr.responseJSON.error.password + "</span>");
                        }
                    }
                   
                    
                });

    }

}




