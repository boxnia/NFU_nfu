<?php
//require(DAO_USERS . "userDAO.class.singleton.php");
//require(MODEL_PATH . "Db.class.singleton.php");


class login_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_user_BLL($arrArgument) {
        
        return $this->dao->create_user_DAO($this->db, $arrArgument);
    }

    public function obtain_paises_BLL($url) {
        return $this->dao->obtain_paises_DAO($url);
    }

    public function obtain_provincias_BLL() {
        return $this->dao->obtain_provincias_DAO();
    }

    public function obtain_poblaciones_BLL($arrArgument) {
        return $this->dao->obtain_poblaciones_DAO($arrArgument);
    }
       public function exist_users_nombre_BLL($arrArgument) {
          
       return $this->dao->exist_users_nombre_DAO($this->db, $arrArgument);
       }
       
          public function details_users_BLL($arrArgument) {
          
       return $this->dao->details_users_DAO($this->db, $arrArgument);
       }
       
        public function exist_users_email_BLL($arrArgument) {
       return $this->dao->exist_users_email_DAO($this->db, $arrArgument);
       }

    public function validate_email_recover_BLL($arrArgument) {
        
       return $this->dao->validate_email_recover_DAO($this->db, $arrArgument);
    }
    
    public function update_email_recover_BLL($arrArgument) {
       return $this->dao->update_email_recover_DAO($this->db, $arrArgument);
    }
    
    public function update_pass_recover_BLL($arrArgument) {
       return $this->dao->update_pass_recover_DAO($this->db, $arrArgument);
    }
    
    
     
    public function users_exist_BLL($arrArgument) {
       return $this->dao->users_exist_DAO($this->db, $arrArgument);
    }
      public function users_exist_codigo_BLL($arrArgument) {
       return $this->dao->users_exist_codigo_DAO($this->db, $arrArgument);
    }
}
