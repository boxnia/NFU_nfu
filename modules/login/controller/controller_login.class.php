<?php

class controller_login {

    public function __construct() {

        include(FUNCTIONS_LOGIN . "functions.inc.php");
        include(UTILS . "upload.inc.php");

        $_SESSION['module'] = "login";
    }

    /////////////////////////////////////////////////// load html alta users
    public function create_users() {

        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");


        loadView('modules/login/view/', 'login.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    /////////////////////////////////////////////////// load html passwords

    public function view_recovery() {
        $_SESSION['header'] = "nulo";
        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");


        loadView('modules/login/view/', 'recovery.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    /////////////////////////////////////////////////// load html email
    public function view_recovery_pass() {

        $_SESSION['header'] = "nulo";
        require_once(VIEW_PATH_INC . "header.html");
        require_once(VIEW_PATH_INC . "menu.php");


        loadView('modules/login/view/', 'recovery_pass.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    /////////////////////////////////////////////////// create_users
    public function alta_users() {


        $jsondata = array();

        $usersJSON = json_decode($_POST["alta_users_json"], true);


        $result = validate_users($usersJSON);

        if (($result['resultado'])) {
            $arrArgument = ($result['datos']['nombre']);

            set_error_handler('ErrorHandler');
            try {

                $exist = loadModel(MODEL_LOGIN, "login_model", "exist_users_nombre", $result['datos']['nombre']);
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();
            if ($exist[0]['total_nombre'] === '0') {
                $arrArgument = ($result['datos']['email']);

                set_error_handler('ErrorHandler');
                try {

                    $exist = loadModel(MODEL_LOGIN, "login_model", "exist_users_email", $arrArgument);
                } catch (Exception $e) {
                    $arrValue = false;
                }
                restore_error_handler();

                if ($exist[0]['total_email'] === '0') {
                    
                    $_SESSION['avatar'] = get_gravatar( ($result['datos']['email']), $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array() );

                    
                    $arrArgument = array(
                        'nombre' => $result['datos']['nombre'],
                        'email' => ($result['datos']['email']),
                        'password' => password_hash($result['datos']['password'], PASSWORD_BCRYPT),
                        //'password2' =>($result['datos']['password2']),
                        'fecha_nac' => ($result['datos']['fecha_nac']),
                        'fecha_registro' => date("Y-m-d"),
                        'token' => md5(uniqid(rand(), true)),
                        'status' => ('0'),
                        'tipo' => ('normal'),
                        'avatar' => $_SESSION['avatar'],
                    );




                    $arrValue = false;


                    set_error_handler('ErrorHandler');
                    try {

                        $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                    } catch (Exception $e) {
                        $arrValue = false;
                    }
                    restore_error_handler();

                    if ($arrValue) {
                        $_SESSION['users'] = $arrArgument;
                        $_SESSION['token'] = $arrArgument['token'];



                        $jsondata["success"] = true;
                        echo json_encode($jsondata);
                        exit;
                    }
                } else {
                    $jsondata["success"] = false;

                    $error['email'] = 'El email ya está registrado';
                    $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                    $jsondata["error"] = $result['error'];

                    header('HTTP/1.0 404 Bad error', true, 404);
                    echo json_encode($jsondata);
                }
            } else {


                $jsondata["success"] = false;

                $error['nombre'] = 'El usuario ya existe';
                $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                $jsondata["error"] = $result['error'];
                header('HTTP/1.0 404 Bad error', true, 404);
                echo json_encode($jsondata);
            }
        } else {

            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];

            $jsondata["success1"] = false;
            if ($result_avatar['resultado']) {
                $jsondata["success1"] = true;
            }


            header('HTTP/1.0 404 Bad error', true, 404);
            echo json_encode($jsondata);
        }
    }

    public function social_facebook() {


        $jsondata = array();

        $usersJSON = json_decode($_POST["date_facebook"], true);


        if ($usersJSON) {

            set_error_handler('ErrorHandler');
            try {

                $exist = loadModel(MODEL_LOGIN, "login_model", "users_exist_codigo", $usersJSON['id']);
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();


      if ($exist[0]['total_codigo'] === '0') {


                if (isset($usersJSON['picture'])) {
                    
                } else {
                    $usersJSON['picture'] = MEDIA_PATH . 'default-avatar.png';
                }
                $arrArgument = array(
                    'nombre' => $usersJSON['nombre'],
                    'email' => ($usersJSON['email']),
                    'fecha_registro' => date("Y-m-d"),
                    'token' => md5(uniqid(rand(), true)),
                    'status' => ('1'),
                    'tipo' => ('normal'),
                    'avatar' => $usersJSON['picture'],
                     'codigo' => $usersJSON['id'],
                );



      
                $arrValue = false;

                set_error_handler('ErrorHandler');
                try {

                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                } catch (Exception $e) {
                    $arrValue = false;
                }
                restore_error_handler();

                if ($arrValue) {

                    $_SESSION['users'] = $arrArgument;
                    $_SESSION['token'] = $arrArgument['token'];

                    $callback = "/users/users";

                    $jsondata["redirect"] = $callback;

                    $jsondata["success"] = true;
                    echo json_encode($jsondata);
                    exit;
                }
            } else {

                set_error_handler('ErrorHandler');
                try {

                    $arrUsers = loadModel(MODEL_LOGIN, "login_model", "users_exist", $usersJSON['nombre'] );
                } catch (Exception $e) {
                    $arrUsers = false;
                }
                restore_error_handler();
 
                if ($arrUsers) {
                    
      
                    $_SESSION['users'] = $arrUsers[0];
                    $_SESSION['token'] = $arrUsers['token'];

                    $_SESSION['users']['avatar'] = $usersJSON['picture'];
                    $callback = "/users/users";

                    $jsondata["redirect"] = $callback;

                    $jsondata["success"] = true;
                    echo json_encode($jsondata);
                    exit;
                } else {

                    $jsondata["success"] = false;
                    $jsondata["error"] = "No se ha encontrado al usuario";

                    $jsondata["success1"] = false;
                    if ($result_avatar['resultado']) {
                        $jsondata["success1"] = true;
                    }
                    echo json_encode($jsondata);
                }
            }
        } else {

            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];

            $jsondata["success1"] = false;
            if ($result_avatar['resultado']) {
                $jsondata["success1"] = true;
            }


            header('HTTP/1.0 404 Bad error', true, 404);
            echo json_encode($jsondata);
        }
    }

    public function social_twiter() {        
        
        $jsondata = array();
        /* logout twiter con logout normal, es decir, con sesion destroy() */
        /* estamos averiguando el porque no entra en la funciones despues de abrirse las peticiones por twitter */

            //fresh authentication
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

            $request_token = $connection->getRequestToken(OAUTH_CALLBACK);

            //received token info from twitter
            $_SESSION['token'] = $request_token['oauth_token'];
            $_SESSION['token_secret'] = $request_token['oauth_token_secret'];



            // any value other than 200 is failure, so continue only if http code is 200
            if ($connection->http_code == '200') {
                
                //redirect user to twitter
                $twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
                $jsondata["success"] = true;
                $jsondata["redirect"] = $twitter_url;
                echo json_encode($jsondata);
                exit;
                /* header('Location: ' . $twitter_url); */
            } else {
                
                $jsondata["success"] = false;
                $jsondata["mensaje"] = "error connecting to twitter! try again later!";
                echo json_encode($jsondata);
                exit;
                /* die("error connecting to twitter! try again later!"); */
                
            }
        
    }
    
    public function social_twiter_return(){
        

        session_start();



        if (isset($_REQUEST['oauth_token']) && $_SESSION['token'] !== $_REQUEST['oauth_token']) {
           
            // if token is old, distroy any session and redirect user to index.php
            session_destroy();
            redirect("/users/users");
            /* amigable('?module=main&function=begin'); */
            /*header('Location: http://51.254.97.198/main/begin');*/
        } elseif (isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token']) {


            // everything looks good, request access token
            //successful response returns oauth_token, oauth_token_secret, user_id, and screen_name
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['token'], $_SESSION['token_secret']);
            $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

            /* utilitzar esta */
            /* $params['include_entities'] = 'false';
              $content = $connection->get('account/verify_credentials', $params); */


            if ($connection->http_code == '200') {

                //redirect user to twitter
                $_SESSION['status'] = 'verified';
                $_SESSION['request_vars'] = $access_token; /* aqui se guadara toda la informacion */
                $usersJSON['id'] = $_SESSION['request_vars']['user_id'];
                /* echo json_encode("linea 313 controller_login.php");
                  die(); */
                /* load model guardar en sesion */
                /* Comprobamos si existe el usuario */
                set_error_handler('ErrorHandler');
                try {
                    $exist = loadModel(MODEL_LOGIN, "login_model", "users_exist_codigo", $usersJSON['id']);
                } catch (Exception $e) {
                    $exist = false;
                }
                
                restore_error_handler();

                /* Fin Comprobamos si existe el usuario */
                /* si existe */

                if ($exist[0]['total_codigo'] === '0') {
                    $_SESSION['avatar'] = get_gravatar(($_SESSION['request_vars']['user_id']), $s = 80, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                    
                    $arrArgument = array(                        
                        'nombre' => $_SESSION['request_vars']['screen_name'],                        
                        'fecha_registro' => date("Y-m-d"),
                        'token' => md5(uniqid(rand(), true)),
                        'status' => ('1'),
                        'tipo' => ('normal'),
                        'avatar' => $_SESSION['avatar'],
                        'codigo' => $_SESSION['request_vars']['user_id'],
                    );



                    $arrValue = false;

                    set_error_handler('ErrorHandler');

                    try {

                        $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                    } catch (Exception $e) {

                        $arrValue = false;
                    }

                    restore_error_handler();

                    if ($arrValue) {
                        $_SESSION['users'] = $arrArgument;                        
                        $_SESSION['token'] = $arrArgument['token'];                        
                        redirect("/users/users");
                    } else {
                        die("error connecting to twitter! try again later!");
                    }
                } else {
                    set_error_handler('ErrorHandler');
                    try {

                        $arrUsers = loadModel(MODEL_LOGIN, "login_model", "users_exist", $_SESSION['request_vars']['screen_name'] );
                    } catch (Exception $e) {
                        $arrUsers = false;
                    }
                    restore_error_handler();
                    
                     if ($arrUsers) {
                    
      
                        $_SESSION['users'] = $arrUsers[0];                        
                        /*$_SESSION['token'] = $arrUsers['token'];*/
                        
                        /*$_SESSION['users']['avatar'] = $arrUsers['avatar'];*/
                        redirect("/users/users");
                        
                    } else {

                        die("usuario no encontrado");
                    }
                                                                                                                                            
                }

                // unset no longer needed request tokens
                unset($_SESSION['token']);
                unset($_SESSION['token_secret']);
                 redirect("/users/users");
              
                /* amigable('?module=users&function=users'); */
            } else {

                die("error, try again later!");
            }
        }

        if (isset($_GET["denied"])) {

            /* no entra */            
            redirect("/main/begin");
            exit;
            /* header('Location: ./index.php');
              die(); */
        }
    }

    public function singin() {


        $result = array();

        $loginJSON = json_decode($_POST["login"], true);



        $result = login($loginJSON);


        if ($result['resultado']) {
            
               
            $arrArgument = $result['datos']['nombre'];
            set_error_handler('ErrorHandler');
            try {
                $exist = loadModel(MODEL_LOGIN, "login_model", "exist_users_nombre", $arrArgument);
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();
            
        
            //si existe el usuario
            if ($exist[0]['total_nombre'] === '1') {


                $arrArgument = $result['datos']['nombre'];

                try {
                    $user = loadModel(MODEL_LOGIN, "login_model", "details_users", $arrArgument);
                } catch (Exception $e) {
                    $arrValue = false;
                }
                restore_error_handler();

                $res = password_verify($result['datos']['password'], $user[0]['password']);





                if ($res) {

                    $_SESSION['users'] = $user[0];

                    $callback = "/users/activarUser/L" . $_SESSION['users']['token'];

                    $jsondata["success"] = true;
                    $jsondata["redirect"] = $callback;
                    echo json_encode($jsondata);
                    exit;
                } else {



                    $jsondata["success"] = false;

                    $error['password'] = 'Contraseña incorrecta';
                    $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                    $jsondata["error"] = $result['error'];

                    header('HTTP/1.0 404 Bad error', true, 404);
                    echo json_encode($jsondata);
                }
            } else {


                $jsondata["success"] = false;

                $error['nombre'] = 'El usuario no está registrado';

                $result = array('resultado' => false, 'error' => $error, 'datos' => '');
                $jsondata["error"] = $result['error'];
                header('HTTP/1.0 404 Bad error', true, 404);
                echo json_encode($jsondata);
            }
        } else {

            $jsondata["success"] = false;
            $jsondata["error"] = $result['error'];
            header('HTTP/1.0 404 Bad error', true, 404);
            echo json_encode($jsondata);
        }
    }

    /////////////////////////////////////////////////// Recovery
    public function email_recovery() {
        /* Comparamos el email, para ver si existe */


        $jsondata = array();

        $email_recovery_JSON = json_decode($_POST["recovery_email_JSON"], true);



        $result = validatemail($email_recovery_JSON['email']);

        if ($result) {
            $arrValue = false;
            set_error_handler('ErrorHandler');
            try {

                $arrValue = loadModel(MODEL_LOGIN, "login_model", "validate_email_recover", $email_recovery_JSON['email']);
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();
        } else {
            $jsondata["success"] = false;
            $jsondata["mensaje"] = "El email introducido no es valido";
            /* header('HTTP/1.0 404 Bad error', true, 404); */
            echo json_encode($jsondata);
        }

        /* Actualizamos el token, y enviamos email */
        if ($arrValue) {
            $arrValue = false;
            $token = md5(uniqid(rand(), true));


            $arr_recovery = array(
                'email' => $email_recovery_JSON['email'],
                'token' => $token,
            );

            set_error_handler('ErrorHandler');
            try {

                $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_email_recover", $arr_recovery);
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();

            if ($arrValue) {

                $arrArgument = array(
                    'type' => 'recovery',
                    'token' => $arr_recovery['token'],
                    'inputName' => 'Name',
                    'inputEmail' => $arr_recovery['email'],
                    'inputSubject' => 'Subject',
                    'inputMessage' => 'Message'
                );

                set_error_handler('ErrorHandler');
                try {
                    if (enviar_email($arrArgument)) {
                        $callback = "/main/begin";
                        $jsondata["success"] = true;
                        $jsondata["mensaje"] = "Su mensaje a sido enviado, compruebe su bandeja de entrada";
                        $jsondata["redirect"] = $callback;
                        echo json_encode($jsondata);
                        /* echo "<div class='alert alert-success'>Su mensaje a sido enviado, compruebe su bandeja de entrada</div>"; */
                    } else {
                        /* $jsondata["success"] = false;        
                          echo json_encode($jsondata); */
                        echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                    }
                } catch (Exception $e) {
                    /* $jsondata["success"] = false;
                      echo json_encode($jsondata); */
                    echo "<div class='alert alert-error'>Error en el servidor, por favor intentelo mas tarde.</div>";
                }
                restore_error_handler();
            } else {
                $jsondata["success"] = false;
                $jsondata["mensaje"] = "Ha habido un problema en el servidor, por favor intentelo mas tarde";
                /* header('HTTP/1.0 404 Bad error', true, 404); */
                echo json_encode($jsondata);
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["mensaje"] = "El email introducido no existe";
            /* header('HTTP/1.0 404 Bad error', true, 404); */
            echo json_encode($jsondata);
        }
    }

    /////////////////////////////////////////////////// Recovery

    public function email_recovery_pass() {

        $jsondata = array();


        $email_recovery_JSON = json_decode($_POST["data_pass_JSON"], true);

        $pass = $email_recovery_JSON['password'];
        $pass2 = $email_recovery_JSON['password2'];


        $result = validar_passwd($pass, $pass2);

        if ($result == "") {

            $pass_crypt = password_hash($pass, PASSWORD_BCRYPT);

            $arr_recovery_pass = array(
                'pass' => $pass_crypt,
                'token' => $_SESSION['users'][0]['token'],
            );

            set_error_handler('ErrorHandler');
            try {

                $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_pass_recover", $arr_recovery_pass);
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();

            if ($arrValue) {

                $callback = "/login/create_users";
                $jsondata["success"] = true;
                $jsondata["mensaje"] = "Se ha modificado su contraseña satisfactoriamente";
                $jsondata["redirect"] = $callback;
                echo json_encode($jsondata);
            } else {
                $jsondata["success"] = false;
                $jsondata["mensaje"] = "Ha habido un problema en el servidor, porfavor intentelo más tarde";
                echo json_encode($jsondata);
            }
        } else {
            $jsondata["success"] = false;
            $jsondata["mensaje"] = $result;
            echo json_encode($jsondata);
        }
    }

    //////////////////////////////////////////////////////////////// close_sesion
    function logout() {

        session_destroy();
        redirect(amigable("?module=main&function=begin", true));
    }

//////////////////////////////////////////////////////////////// load
    public function load_users() {

        if (isset($_POST["load"]) && $_POST["load"] == true) {

            $jsondata = array();
            if (isset($_SESSION['users'])) {
//echo debug($_SESSION['user']);
                $jsondata["users"] = $_SESSION['users'];
            }
            if (isset($_SESSION['msje'])) {
//echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }

            if ($jsondata) {

                close_session(); ////////////////////////// en utils/utils.inc.php
                echo json_encode($jsondata);
                exit;
            } else {
                close_session(); ////////////////////////// en utils/utils.inc.php
                showErrorPage(1, "", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

/////////////////////////////////////////////////// cargar usuario
    public function load_data_users() {
        if ((isset($_POST["load_data"])) && ($_POST["load_data"] == true)) {
            $jsondata = array();

            if (isset($_SESSION['users'])) {
                $jsondata["users"] = $_SESSION['users'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["users"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

/////////////////////////////////////////////////// load_pais

    public function load_pais_users() {

        if ((isset($_POST["load_pais"])) && ($_POST["load_pais"] == true)) {
//  echo($_POST["load_pais"]);

            $json = array();

            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';

//$path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/users/model/model/';

            set_error_handler('ErrorHandler');
            try {

                $json = loadModel(MODEL_LOGIN, "users_model", "obtain_paises", $url);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();
            if ($json) {
                echo $json;
                exit;
            } else {
                $json = "error";
                echo $json;
                exit;
            }
        }
    }

/////////////////////////////////////////////////// load_provincias
    public function load_provincias_users() {
        if ((isset($_POST["load_provincias"])) && ($_POST["load_provincias"] == true)) {
            $jsondata = array();
            $json = array();

// $path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/users/model/model/';
            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_LOGIN, "users_model", "obtain_provincias");
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();
            if ($json) {
                $jsondata["provincias"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["provincias"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

/////////////////////////////////////////////////// load_poblaciones
    public function load_poblaciones_users() {
        if (isset($_POST['idPoblac'])) {
            $jsondata = array();
            $json = array();

// $path_model = $_SERVER['DOCUMENT_ROOT'] . '/PhpProject_6/modules/users/model/model/';
            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_LOGIN, "users_model", "obtain_poblaciones", $_POST['idPoblac']);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();
            if ($json) {
                $jsondata["poblaciones"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["poblaciones"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

/////////////************enviar email**********/////////

    public function process_contact() {
        if ($_POST['token'] === "formulario") {

//////////////// Envio del correo al usuario
            $arrArgument = array(
                'type' => 'alta',
                'token' => $_SESSION['token'],
                'inputName' => $_POST['nombre'],
                'inputEmail' => $_POST['email'],
            );


            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {
//echo($arrArgument);

                    echo("Consulte su bandeja de entrada para finalizar registro");
                } else {
                    echo ("Server error. Intentar más tarde...");
                }
            } catch (Exception $e) {
                echo "Server error. Intentar más tarde...";
            }
            restore_error_handler();


//////////////// Envio del correo al admin de la ap web
            $arrArgument = array(
                'type' => 'admin',
                'token' => $_SESSION['token'],
                'inputName' => $_POST['nombre'],
                'inputEmail' => $_POST['email'],
            );
            set_error_handler('ErrorHandler');
            try {
                if (enviar_email($arrArgument)) {

//  echo "<div class='alert alert-success'>Your message has been sent </div>";
                } else {
                    echo "<div class='alert alert-error'>Server error. Try later...</div>";
                }
            } catch (Exception $e) {
                echo "<div class='alert alert-error'>Server error. Try later...</div>";
            }
            restore_error_handler();
        } else {
            echo "<div class='alert alert-error'>Server error. Try later...</div>";
        }
    }

}
