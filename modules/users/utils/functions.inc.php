<?php

function validate_users($value) {


    $error = array();
    $valido = true;
    $error1 = "";

    $error2 = "";
    $filtro = array(
        
        'password' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{6,12}$/')
        ),
        'password2' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{6,12}$/')
        ),
        'fecha_nac' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
        ),
        'telefono' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[9|6|7][0-9]{8}$/')
        ), 
    );

    $resultado = filter_var_array($value, $filtro);
  
    if ($resultado != null && $resultado) {


        if ( $resultado['password'] != $_POST['password2']) {
            $error['password'] = 'Password debe tener de 6 a 12 caracteres y las dos contrasenyas deben ser iguales';
            $resultado['password'] = $_POST['password'];
            $valido = false;
        }

 

        $datetime = date("m/d/Y");
   
   
        if ($value['fecha_nac'] === "" || !compruebaFecha($value['fecha_nac']) || !valida_dates($value['fecha_nac'], $datetime)) {
        
     
            $error['fecha_nac'] = 'El usuario debe ser mayor de edad';
            $error['fecha_registro'] = 'El campo esta vacio';
            $resultado['fecha_nac'] = $value['fecha_nac'];
            $valido = false;
        } else if (!compruebaFecha($value['fecha_nac'])) {

            $error['fecha_nac'] = 'La fecha no es válida';
            $valido = false;
        } else {
            $resultado['fecha_nac'] = $value['fecha_nac'];
            $resultado['fecha_registro'] = $value['fecha_registro'];
            $valido = true;
        }
    } else {
        $valido = false;
    }

    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}

function valida_dates($start_day, $daylight) {
    // list($mes_one, $dia_one, $anio_one) = split('/', $start_day);
    //list($mes_two, $dia_two, $anio_two)= split('/', $daylight);

       
    $date = date_create_from_format("m/d/Y", $start_day);


    $myDateTime = date_format($date, "Y/m/d");


    $date1 = date_create_from_format("m/d/Y", $daylight);


    $myDateTime1 = date_format($date1, "Y/m/d");





    $newDate = strtotime($myDateTime);
    $newDate1 = strtotime($myDateTime1);



    $days_between = ceil(abs($newDate - $newDate1) / 84600);

    //pongo 365 para no buscar la fecha, para ser mayor de 18 años, tendría que ser mayor de 6570 días.
    if ($days_between > 6570) {
        return true;
    }
    return false;

   
}

function validatemail($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }
    return false;
}

function validar_facilities_checked($facilities) {
    $_error = "";

    if (empty($facilities)) {
        return $_error = "Seleccione al menos dos opciones";
    } else {

        $no_checked = count($facilities);
        if ($no_checked < 2)
            return $_error = "Seleccione al menos dos opciones";
    }
    return $_error = "";
}

function validar_passwd($passwd, $passwd2) {
    $errorpassw = "";
    if (strlen($passwd) < 6) {
        return $errorpassw = "Password debe tener al menos 6 caracteres";
    }
    if (strlen($passwd) > 16) {
        return $errorpassw = "Password no puede tener m�s de 16 caracteres";
    }
    if (!preg_match('`[a-z]`', $passwd)) {
        return $errorpassw = "Password debe tener al menos una letra minúscula";
    }
    if (!preg_match('`[A-Z]`', $passwd)) {
        return $errorpassw = "Password debe tener al menos una letra mayúscula";
    }
    if (!preg_match('`[0-9]`', $passwd)) {
        return $errorpassw = "Password debe tener al menos un caracter numérico";
    }

    if ($passwd != $passwd2) {
        return $errorpassw = "Los password deben coincidir";
    }
    return $errorpassw = "";
}

function compruebaFecha($date) {

    $test_arr = explode('/', $date);
    if (count($test_arr) == 3) {
        if (checkdate($test_arr[0], $test_arr[1], $test_arr[2])) {
            // valid date ...
            return true;
        } else {
            // problem with dates ...
            return false;
        }
    } else {
        return false;
        // problem with input ...
    }
}

function validate_poblacion($poblacion) {
    //$poblacion = addslashes($poblacion);
    $poblacion = filter_var($poblacion, FILTER_SANITIZE_STRING);
    return $poblacion;
}


function get_user($token){
    set_error_handler('ErrorHandler');
            try {

                $arrValue = loadModel(MODEL_USERS, "users_model", "obtain_user", $token);
                
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();

            if ($arrValue) {
               
                $_SESSION['users'] = $arrValue;
                
         
            } 
}