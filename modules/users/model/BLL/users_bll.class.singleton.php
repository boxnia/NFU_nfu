<?php
//require(DAO_USERS . "userDAO.class.singleton.php");
//require(MODEL_PATH . "Db.class.singleton.php");


class users_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = users_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function update_user_BLL($arrArgument) {
        
        return $this->dao->update_user_DAO($this->db, $arrArgument);
    }
    
    public function obtain_users_BLL() {
        
        return $this->dao->obtain_users_DAO($this->db);
    }
    
    public function activateUser_BLL($arrArgument) {
         
        return $this->dao->activateUser_DAO($this->db, $arrArgument);
    }

    public function obtain_paises_BLL($url) {
        return $this->dao->obtain_paises_DAO($url);
    }

    public function obtain_provincias_BLL() {
        return $this->dao->obtain_provincias_DAO();
    }

    public function obtain_poblaciones_BLL($arrArgument) {
        return $this->dao->obtain_poblaciones_DAO($arrArgument);
    }
    
    public function obtain_coord_BLL() {
        return $this->dao->obtain_coord_DAO($this->db);
    }
     public function obtain_coord_near_BLL($arrArgument) {
        return $this->dao->obtain_coord_near_DAO($this->db,$arrArgument);
    }
      public function obtain_coord_near_map_BLL($arrArgument) {
        return $this->dao->obtain_coord_near_map_DAO($this->db,$arrArgument);
    }
     public function obtain_user_BLL($arrArgument) {
        return $this->dao->obtain_user_DAO($this->db,$arrArgument);
    }
     public function count_games_BLL() {
        return $this->dao->count_games_DAO($this->db);
    }

}
