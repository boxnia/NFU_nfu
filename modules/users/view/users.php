<script type="text/javascript" src="<?php echo USERS_JS_PATH ?>users.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>form-elements.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo VIEW_PATH_FORM ?>font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo USERS_CSS_PATH ?>styles.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css">
<body>
    <div class="top-content">
        <div class="container">
            <div class="row">





                <div class="form-group">
                    <h2 id="mi_perfil_" class="last_match">EDITAR DATOS PERSONALES</h2>
                </div>
                <hr class="small">
                <form class="form-horizontal" name="formulario"  id="formulario" >
                    <div class="form-box" id="form-box">

                        <div class="col-sm-6">
                            <label  class="last_match" id="mi_perfil" >Datos Personales:</label></br>
                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Usuario:</label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Usuario" disabled>
                                </div>
                            </div>
                             <div class="form-group">
                                  <div class="col-xs-9">
                            <label  class="last_match" id="show_password" >&nbsp;Modificar contraseña</label></br>
                              </div>
                             </div>
                            <div class="form-group" id="form_hidden">
                                 
                                <div class="form-group">
                                    <label class="control-label col-xs-3" id="label_mi_perfil" >Password actual:</label>
                                    <div class="col-xs-9">
                                        <input type="password" class="form-control" id="password_old" name="password_old" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" id="label_mi_perfil" >Modificar Password:</label>
                                    <div class="col-xs-9">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-3" id="label_mi_perfil" >Confirmar Password:</label>
                                    <div class="col-xs-9">
                                        <input type="password" class="form-control" id="password2" name="password2" placeholder="Confirmar Password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Genero:</label>
                                <div class="col-xs-2">

                                    <input type="radio" name="sexo" id="sexo1" value="masculino"> Masculino

                                </div>
                                <div class="col-xs-2">

                                    <input type="radio" name="sexo" id="sexo2" value="femenino"> Femenino

                                </div>
                            </div>



                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >F. Nacimiento:</label>
                                <div class="col-xs-9">
                                    <input id="fecha_nac" type="text" name="fecha_nac" placeholder="Fecha de Nacimiento"  id="fecha_nac"  required value="<?php echo $_POST ? $_POST['fecha_nac'] : ""; ?>" >
                                </div>

                            </div>



                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Cambia imagen perfil</label>
                                <div class="col-xs-9">
                                    <span id="e_avatar" class="styerror"><br></span>
                                    <div id="dropzone" class="dropzone">
                                    </div>
                                    <div id="progress">
                                        <div id="bar"></div>
                                        <div id="percent"></div>
                                    </div>
                                    <br>
                                    <div class="msg"></div>

                                </div>
                            </div>

                        </div>  





                        <div class="col-sm-1"></div>

                        <div class="col-sm-6">


                            <label class="last_match" id="mi_perfil" >Por dónde juegas:</label></br>

                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >País:</label>
                                <div class="col-xs-9">
                                    <select class="form-control" name="pais" id="pais" placeholder="País"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Provincia:</label>
                                <div class="col-xs-9">
                                    <select  class="form-control" name="provincia" id="provincia"  placeholder="Provincia"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Ciudad</label>
                                <div class="col-xs-9">
                                    <select type="text" class="form-control" name="poblacion" placeholder="Poblacion" id="poblacion"></select>
                                </div>
                            </div>
                            <label class="last_match" id="mi_perfil" >Contacto:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></br>
                            <div class="form-group">

                                <label class="control-label col-xs-3" id="label_mi_perfil" >Teléfono:</label>
                                <div class="col-xs-9">
                                    <input type="tel" class="form-control" placeholder="Teléfono" id="telefono" name="telefono">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Email:</label>
                                <div class="col-xs-9">
                                    <input type="email" class="form-control" placeholder="Email" id="email" name="email" >
                                </div>
                            </div>
                            <label class="last_match" id="mi_perfil" >Elige tus deportes:</label></br>
                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Nivel:</label>
                                <div class="col-xs-2">

                                    <input type="radio" name="nivel" id="nivelB" value="bajo"> Bajo

                                </div>
                                <div class="col-xs-2">

                                    <input type="radio" name="nivel" id="nivelM" value="medio"> Medio

                                </div>
                                <div class="col-xs-2">

                                    <input type="radio" name="nivel" id="nivelA" value="alto"> Alto

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" id="label_mi_perfil" >Preferencias deportes:</label>
                                <div class="col-xs-2">
                                    Todos<input type="checkbox" name="deportes[]" class="deportes" id="todos" value="Todos">
                                </div>
                                <div class="col-xs-2">
                                    Fútbol <input type="checkbox" name="deportes[]" class="deportes"  id="futbol"value="futbol">
                                </div>
                                <div class="col-xs-2">
                                    Basket<input type="checkbox" name="deportes[]" class="deportes"  id="basket"value="baloncesto">
                                </div>
                                <div class="col-xs-2">

                                    Voleibol<input type="checkbox" name="deportes[]"class="deportes" id="volei" value="voleibol">

                                </div>
                                <div class="col-xs-2">
                                    Tenis<input type="checkbox" name="deportes[]" class="deportes"  id="tenis" value="tenis">
                                </div>
                                <div class="col-xs-2">

                                    Padel<input type="checkbox" name="deportes[]" class="deportes"  id="padel" value="padel">

                                </div>

                            </div>
                            <br>


                        </div>

                        <div class="col-xs-offset-3 col-xs-9" id="margin_bottons">
                            <input type="button" class="btn btn-primary" name="boton" id="boton" value="Enviar">
                            <input type="reset" class="btn btn-default" value="Limpiar" >
                        </div>


                    </div>
                </form>

            </div>


        </div>


    </div>


</body>
